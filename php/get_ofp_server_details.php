<?php

/**
 * Returns details of Operation Flashpoint server as JSON
 *
 * The script communicates using UDP socket with OFP server
 * specified by GET parameters in format "server=ip_address:port".
 *
 * @author Przemek_kondor at forums.bistudio.com
 * @author Poweruser at forums.bistudio.com - the author of OFPMonitor which was the reference
 * @license -
 * @version 1.0.0
 * @link http://gliniak.ovh.org/webgl/ofp/ofp.html
 * @category Gaming
 * @package OperationFlashpointServers
 */

header("Content-Type: application/json");
require "gamemaster_communication.php";

set_time_limit(8);

$serverIpParts = explode(":", $_GET["server"]);
$ip = $serverIpParts[0];
$port = $serverIpParts[1];

if (!($socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
    echo getSocketErrorJson("Couldn't create UDP socket");
    exit();
}

// ask for fetails

$query = OFP_DETAILS_QUERY;
//if (($sent = socket_write($socket, $query, strlen($query))) === false) {
if (($sent = socket_sendto($socket, $query, strlen($query), 0 , $ip, $port)) === FALSE) {
    echo getSocketErrorJson("Could not send details query to $ip:$port");
    exit();
}

//echo "sent $ip:$port $sent bytes\n";
//echo "full message is ".strlen($query). " bytes\n";
//echo "sent " . $query ."\n";

if (($reply = socket_read($socket, 1024, PHP_BINARY_READ)) === FALSE) {
//if (socket_recv($socket, $buffer, 2045, MSG_WAITALL) === FALSE) {
//if (socket_recvfrom($socket, $reply, 1024, MSG_WAITALL, $ip, $port) === FALSE) {
    echo getSocketErrorJson("Could not read servers details $ip:$port");
    exit();
}

//echo "Reply : $reply";
echo getServerDetailsJson($reply);

socket_close($socket);

?>
