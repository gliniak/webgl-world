<?php

/**
 * Returns list of Operation Flashpoint servers as JSON
 *
 * The script connects to gamemaster server's TCP socket
 * specified by GET parameters in format "masterUrl=ip_address:port".
 * Only Operation Flashpoint: Resistance and Arma: Cold War Assault are supported.
 *
 * @author Przemek_kondor at forums.bistudio.com
 * @author Poweruser at forums.bistudio.com - the modified protocol alghoritm
 * @license -
 * @version 1.0.0
 * @link http://gliniak.ovh.org/webgl/ofp/ofp.html
 * @category Gaming
 * @package OperationFlashpointServers
 */

header("Content-Type: application/json");
require "gamemaster_communication.php";

set_time_limit(10);

$masterIpParts = explode(":", $_GET["masterUrl"]);
$masterIp = $masterIpParts[0];
$masterPort = $masterIpParts[1];

if (!($socket = socket_create(AF_INET, SOCK_STREAM, 0))) {
    echo getSocketErrorJson("Couldn't create socket");
    exit();
}

if (!socket_connect($socket, $masterIp, $masterPort)) {
    echo getSocketErrorJson("Could not connect");
    exit(0);
}

if (!($challengeResponse = socket_read($socket, 2048))) {
    echo getSocketErrorJson("Could not read the challenge");
    exit();
}

$challengeKey = getChallengeKey($challengeResponse);
//echo "challengeKey: $challengeResponse -> $challengeKey\n";

$requestMessage = createRequestMessage($challengeKey);
$requestMessageLength = strlen($requestMessage);

if (($sentCount = socket_write($socket, $requestMessage, strlen($requestMessage))) === false) {
    echo getSocketErrorJson("Could not send message");
    exit();
}

$buffer = "";
$counter = 0;
$recvValue = 0;
// for some reason below line doesn't get expected data
//$serversResponse = ""; while ($counter < 4 and ($serversResponse = socket_read($socket, 1, PHP_BINARY_READ) !== false)) {
while ($counter < 4 and false !== ($recvValue = socket_recv($socket, $buffer, 1, MSG_WAITALL))) {
    //echo "recvValue: $recvValue, buffer: $buffer -> " . ord($buffer) . "\n";

    // according to Poweruser some garbage might come hence here's filter
    if (ord($buffer) == 255) {
        $counter++;
    }
}
if ($recvValue === false) {
    echo getSocketErrorJson("Could not read servers list 1");
    exit();
}

if (($recvValue = socket_recv($socket, $buffer, 8, MSG_WAITALL)) === false) {
    echo getSocketErrorJson("Could not read servers list");
    exit();
}

//TODO this assumes that there is max 255 servers
$ipv4Count = ord($buffer[3]) / 6;

$ipv4Array = array();
for ($i = 0; $i < $ipv4Count; $i++) {
    $serverIp = "";
    if ((socket_recv($socket, $serverIp, 6, MSG_WAITALL)) === false) {
        echo getSocketErrorJson("Could not read servers $i ip");
        exit();
    }
    $ip = ord($serverIp[0]).".".ord($serverIp[1]).".".ord($serverIp[2]).".".ord($serverIp[3]).
                ":".(256 * ord($serverIp[4]) + ord($serverIp[5]));
    array_push($ipv4Array, $ip);
}

echo getServersJson($ipv4Array);

socket_close($socket);

?>
