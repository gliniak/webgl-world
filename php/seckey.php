<?php
function gsvalfunc($reg){
if($reg < 26) return chr($reg + 65);
if($reg < 52) return chr($reg + 71);
if($reg < 62) return chr($reg -4);
if($reg == 62) return('+');
if($reg == 63) return('/');
return(0);
}

function gsseckey($src,$key,$enctype)
{
$enctype1_data=array(1,186,250,178,81,0,84,128,117,22,142,142,2,8,54,165,45,5,13,22,82,7,180,34,140,233,9,214,185,38,0,4,6,5,0,19,24,196,30,91,29,118,116,252,80,81,6,22,0,81,40,0,4,
10,41,120,81,0,1,17,82,22,6,74,32,132,1,162,30,22,71,22,50,81,154,196,3,42,115,225,45,79,24,75,147,76,15,57,10,0,4,192,18,12,154,94,2,179,24,184,7,12,205,33,5,192,169,65,67,4,60,82,117,236,152,128,29,8,2,29,88,132,1,78,59,106,83,122,85,86,87,30,127,236,184,173,0,112,31,130,216,252,151,139,240,131,254,14,118,3,190,57,41,119,48,224,43,255,183,158,1,4,248,1,14,232,83,255,148,12,178,69,158,10,199,6,24,1,100,176,3,152,1,235,2,176,1,180,18,73,7,31,95,94,93,160,79,91,160,90,89,88,207,82,84,208,184,52,2,252,14,66,41,184,218,0,186,177,240,18,253,35,174,182,69,169,187,6,184,136,20,36,169,0,20,203,36,18,174,204,87,86,238,253,8,48,217,253,139,62,10,132,70,250,119,184);

/* 1) buffer creation with incremental data */
	$enctmp=array();

	for($i = 0; $i < 256; $i++){
		$enctmp[$i]=$i;
	}

/* 2) buffer scrambled with key */

	$keysz = strlen($key);

	for($i = $num = 0; $i < 256; $i++){
		$num = ($num + $enctmp[$i] + ord($key[$i % $keysz])) & 0xff;
		$x = $enctmp[$num];
		$enctmp[$num] = $enctmp[$i];
		$enctmp[$i] = $x;
	}

/* 3) source string scrambled with the buffer */

	$num = $num2 = 0;
	$zob=array();
	$size=strlen($src);

	for($i = 0; $i < $size; $i++){

		$num = ($num + ord($src[$i]) + 1) & 0xff;
		$x = $enctmp[$num];
		$num2 = ($num2 + $x) & 0xff;
		$y = $enctmp[$num2];
		$enctmp[$num2] = $x;
		$enctmp[$num] = $y;
		$zob[$i] = ord($src[$i]) ^ $enctmp[($x + $y) & 0xff];
	}


/* 4) enctype management */

	if($enctype == 1){
		for($i = 0; $i < $size; $i++) {
			$zob[$i] = $enctype1_data[$zob[$i]];
		}
	}
	else if($enctype == 2) {
		for($i = 0; $i < $size; $i++) {
			$zob[$i] = $zob[$i] ^ ord($key[$i % $keysz]);
		}
	}

/* 5) splitting of the source string from 3 to 4 bytes */

	$dst='';
	$size /= 3;
	$i=0;
	while($size--) {
		$x = $zob[$i++];
		$y = $zob[$i++];
		$dst .= gsvalfunc($x >> 2);
		$dst .= gsvalfunc((($x & 3) << 4) | ($y >> 4));
		$x = $zob[$i++];
		$dst .= gsvalfunc((($y & 15) << 2) | ($x >> 6));
		$dst .= gsvalfunc($x & 63);
	}
	return $dst;
}