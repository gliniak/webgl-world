﻿<?php

require 'seckey.php';

define("OFP_GAME_NAME", "opflashr");
define("OFP_GAME_KEY", "Y3k7x1");
define("OFP_GAME_ENCTYPE", 2);
define("OFP_FULL_DETAILS_QUERY", "\\info\\rules\\players\\");
define("OFP_DETAILS_QUERY", "\\info\\rules\\");

/**
 * Constructs output JSON with error comming from socket operations
 *
 * @param string $comment additional comment
 *
 * @return string JSON with set error property
 */
function getSocketErrorJson($comment = "")
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
    $errorStr = "$comment | $errorcode | $errormsg";
    $out = '{ "error" : "'. $errorStr .'" }';
    // unfortunately json_encode doesn't work with polish letters
    return $out;// json_encode($out,  JSON_FORCE_OBJECT);
};

/**
 * Constructs output JSON with servers list in "servers" property
 *
 * @param array $serversIp array of servers IP
 *
 * @return string JSON with servers and empty error properties
 */
function getServersJson($serversIp)
{
    $serversIp = array_unique($serversIp);
    $out = array(
        "error" => "",
        "servers" => $serversIp
    );
    return json_encode($out/*, JSON_PRETTY_PRINT*/);
};

/**
 * Constructs JSON with servers details in "details" property and empty "error" property
 *
 * @param string $details
 *
 * @return string JSON with details
 */
function getServerDetailsJson($details)
{
    $out = array(
            "error" => "",
            "details" => $details
    );
    $out = array_map('utf8_encode', $out);
    return json_encode($out);
};

/**
 * Gets challenge key from challenge server response
 *
 * @param string $challengeResponse initial server response
 *
 * @return string challenge key
 */
function getChallengeKey($challengeResponse)
{
    $challengeResponsePrefixLength = strlen("\\basic\\\\secure\\");
    return substr($challengeResponse, $challengeResponsePrefixLength);
}

/**
 * Creates message to the server which requests OFP servers list
 *
 * @param string $challengeKey from initial server response
 *
 * @return string request message
 */
function createRequestMessage($challengeKey)
{
    //g.gsseckey("HpWx9z", "Y3k7x1", 2); -> "h702Gu1a"
    //g.gsseckey("HpWx9z", "UebAWH", 2); -> 'C1SdpLxM"
    //echo "TEST:" . gsseckey("HpWx9z", "UebAWH", 2) . "\n";

    $enctype = OFP_GAME_ENCTYPE;
    $validate = gsseckey($challengeKey, OFP_GAME_KEY, $enctype);
    //echo "validate:". $validate . "\n";
    $reply = "\\gamename\\";
    $reply .= OFP_GAME_NAME;
    $reply .= "\\gamever\\1.8\\location\\0\\validate\\";
    $reply .= $validate;
    $reply .= "\\enctype\\";
    $reply .= $enctype;
    $reply .= "\\final\\\\queryid\\1.1";
    $query = "\\list\\cmp\\gamename\\";
    $query .= OFP_GAME_NAME;
    $query .= "\\enctype\\";
    $enctype = "3";
    $query .= $enctype;
    $query .= "\\where\\\\final\\";
    $reply .= $query;
    //echo $reply . "\n";
    return $reply;
}

/* use this when single socket_write doesn't work sometimes due to weak connection etc.
note that socket_write doesn't necessary send whole message at once
$sentCount = 0;
$sendAttempts = 0;

while ($sendAttempts < 1000 and
    ($sentCount = socket_write($socket, $requestMessage, $requestMessageLength)) !== false) {
    echo "sent $sentCount, ";
    $sendAttempts++;
}
if ($sentCount === false) {
    echo getSocketErrorJson("Could not send message");
    exit();
}*/

?>
