"use strict";

wglb.prepareNs("WGLE");

/**
 * Perspective camera class
 * @param {Array} aConfiguration.target - 3-elements array representing initial position of camera target (by default [0, 0, 0])
 * @param {Array} aConfiguration.position - 3-elements array representing initial position of camera (by default [0, 0, 1])
 * @param {Array} aConfiguration.up - 3-elements array representing initial up-vector of camera (by default [0, 1, 0])
 * @param {WGLE.Renderer} aConfiguration.renderer
 * @constructor
 */
WGLE.Camera = function(aConfiguration) {
    if (!aConfiguration || !(aConfiguration.renderer instanceof WGLE.Renderer))
        throw new Error("Missing configuration and/or renderer");

    /**
     * Renderer object
     * @type WGLE.Renderer
     */
    this.renderer = aConfiguration.renderer;

    /**
     * Field of view
     * @type Number
     */
    this.fov = 60.0;

    /**
     * Position of camera target. To set new target use lookAt function.
     * @type vec3
     */
    this.target = vec3.create(aConfiguration.target || [0, 0, 0]);

    /**
     * Position of the camera. To set new position use lookAt function.
     * @type vec3
     */
    this.position = vec3.create(aConfiguration.position || [0, 0, 1]);

    /**
     * Up vector of camera. To set new up vector use lookAt function.
     * @type vec3
     */
    this.up = vec3.create(aConfiguration.up || [0, 1, 0]);

    /**
     * Unified direction vector (from position to target)
     */
    this._direction = vec3.create();

    /**
     * Holds forward step vector
     */
    this._forwardVector = vec3.create();

    /**
     * Holds backward step vector
     */
    this._backVector = vec3.create();

    /**
     * Holds left step vector
     */
    this._leftVector = vec3.create();

    /**
     * Holds right step vector
     */
    this._rightVector = vec3.create();

    /**
     * Model view matrix holing camera transformations
     * @type mat4
     */
    this.mvMatrix = mat4.create();

    /**
     * Sensitivity of the mouse moving
     */
    this._mouseSensitivity = 2 * Math.PI / 1000;

    /**
     * Sensitivity factor for the keyboard
     */
    this._keyboardSensitivity = 0.1;

    /**
     * Whether the update of mvMatrix needs to be done by calling lookAt.
     * This property is used to avoid redundant calling of mat4.lookAt when none of position, target and up didn't changed
     */
    this._mustUpdate = true;

    this.lookAt(this.position, this.target, this.up);
    this._installEventsHandlers(this.renderer.eventsManager);
    this.rotate(0, 0);
};

/**
 * @param {Array} aPosition - new position of camera
 * @param {Array} aTarget - position of camera target
 * @param {Array} [aUp=vec3(0, 1, 0)] - up vector
 */
WGLE.Camera.prototype.lookAt = function(aPosition, aTarget, aUp) {
    mat4.lookAt(this.position = aPosition, this.target = aTarget, this.up = aUp || [0, 1, 0], this.mvMatrix);
    vec3.direction(this.position, this.target, this._direction);
    this._mustUpdate = false;
    //vec3.subtract(this.position, this.target, this._direction);
    //vec3.normalize(this._direction);
};

/**
 * Updates camera view matrix according to changes in position, target and up properties
 */
WGLE.Camera.prototype.apply = function() {
    // update only if needed
    this._mustUpdate && mat4.lookAt(this.position, this.target, this.up, this.mvMatrix);
    this._mustUpdate = false;
};

/**
 * Rotates camera
 * @param {Number} aAngleY - angle in radians around Y-axis
 * @param {Number} aAngleX - angle in radians around X-axis
 */
WGLE.Camera.prototype.rotate = function(aAngleY, aAngleX) {
    aAngleY *= this._mouseSensitivity;
    aAngleX *= this._mouseSensitivity;
    //console.debug("rotate", aAngleY, aAngleX);
    var rotMatrix = mat4.create();
    mat4.identity(rotMatrix);
    mat4.rotateY(rotMatrix, aAngleY);
    mat4.multiplyVec3(rotMatrix, this._direction);
    vec3.add(this.position, this._direction, this.target);

    // calculate forward, backward, left and right vectors too
    this._updateDirectionVectors();

    this._mustUpdate = true;
};

/**
 * Moves camera toward its target.
 * @param {vec3} aVector - direction vector.
 */
WGLE.Camera.prototype.move = function(aVector) {
    //vec3.scale(this._forwardVector, aDistance || 1.0, this._moveStep);
    vec3.add(this.position, aVector, this.position);
    vec3.add(this.target, aVector, this.target);
    //console.debug("move", vec3.str(this.position), vec3.str(this.target));
    this._mustUpdate = true;
};

/**
 * Zooms - changes field of view
 * @param {Boolean} aZoomIn - whether to zoom in or zoom out
 */
WGLE.Camera.prototype.zoom = function(aZoomIn) {
    //console.debug(this, aZoomIn);
    this.fov *= aZoomIn ? 1.05 : 0.95;
    this._mustUpdate = true;
    this.renderer.updatePerspective();
};

/**
 * Updates forward, back, right and left vectors
 */
WGLE.Camera.prototype._updateDirectionVectors = function() {
    vec3.scale(this._direction, this._keyboardSensitivity, this._forwardVector);
    vec3.negate(this._forwardVector, this._backVector);
    vec3.cross(this._direction, this.up, this._rightVector);
    vec3.scale(this._rightVector, this._keyboardSensitivity, this._rightVector);
    vec3.negate(this._rightVector, this._leftVector);
};

/**
 * Adds handlers for events managed by EventsManager
 * @param {WGLE.EventsManager} aEventsManager
 * @private
 */
WGLE.Camera.prototype._installEventsHandlers = function(aEventsManager) {
    if ((!aEventsManager instanceof WGLE.EventsManager)) throw new Exception("EventsManager is missing");

    aEventsManager.scrollEventObservers.push(this.zoom.bind(this));
    aEventsManager.panEventObservers.push(this.rotate.bind(this));
    aEventsManager.clickEventObservers.push(this._handleKeyboard.bind(this));
};

/**
 * Handles keyboard clicks
 * @param {Array} aKeys - holds keycode - Boolean map
 * @private
 */
WGLE.Camera.prototype._handleKeyboard = function(aKeys) {
    //console.debug("camera._handleKeyboard");
    //var shift = aKeys[WGLE.EventsManager.SHIFT_KEY];

    // left
    if (aKeys[WGLE.EventsManager.LEFT_KEY] || aKeys[WGLE.EventsManager.A_KEY]) {
        this.move(this._leftVector);
    }

    // right
    if (aKeys[WGLE.EventsManager.RIGHT_KEY] || aKeys[WGLE.EventsManager.D_KEY]) {
        this.move(this._rightVector);
    }

    // forward
    if (aKeys[WGLE.EventsManager.UP_KEY] || aKeys[WGLE.EventsManager.W_KEY]) {
        this.move(this._forwardVector);
    }

    // back
    if (aKeys[WGLE.EventsManager.DOWN_KEY] || aKeys[WGLE.EventsManager.S_KEY]) {
        this.move(this._backVector);
    }

    // up
    if (aKeys[WGLE.EventsManager.Q_KEY]) {
        this.move([0, this._keyboardSensitivity, 0]);
    }

    // down
    if (aKeys[WGLE.EventsManager.Z_KEY]) {
        this.move([0, -this._keyboardSensitivity, 0]);
    }

    // make camera movement faster
    if (aKeys[WGLE.EventsManager.PAGE_UP]) {
        this._keyboardSensitivity *= 1.05;
        this._updateDirectionVectors();
    }

    // make camera movement slower
    if (aKeys[WGLE.EventsManager.PAGE_DOWN]) {
        this._keyboardSensitivity /= 1.05;
        this._updateDirectionVectors();
    }
};
