"use strict";

wglb.prepareNs("WGLE.geometry");

/**
 * Net of rectangles geometry. Centers in [0, 0, 0] in XZ plane
 * @param {WebGLRenderingContext} aConfiguration.gl - required WebGL context
 * @param {Number} aConfiguration.xWidth - x width of the segment (1.0 by default)
 * @param {Number} aConfiguration.zWidth - z width of the segment (1.0 by default)
 * @param {Number} aConfiguration.xSegments - number of segments along 0X axis
 * @param {Number} aConfiguration.zSegments - number of segments along 0Z axis
 * @constructor
 */
WGLE.geometry.Net = function(aConfiguration) {
    aConfiguration = aConfiguration || {};

    var pos = [],
        normals = [],
        xWidth = aConfiguration.xWidth || 1.0,
        zWidth = aConfiguration.zWidth || 1.0,
        xSegments = aConfiguration.xSegments || 10,
        zSegments = aConfiguration.zSegments || 10;

    for (var x = 0; x <= xSegments; x++) {
        pos.push(x * xWidth - xWidth * xSegments / 2); pos.push(0); pos.push(zSegments * zWidth / - 2);
        normals.push(0); normals.push(1); normals.push(0);
        pos.push(x * xWidth - xWidth * xSegments / 2); pos.push(0); pos.push(zSegments * zWidth / 2);
        normals.push(0); normals.push(1); normals.push(0);
    }

    for (var z = 0; z <= zSegments; z++) {
        pos.push(xWidth * xSegments / -2); pos.push(0); pos.push(z * zWidth - zSegments * zWidth / 2);
        normals.push(0); normals.push(1); normals.push(0);
        pos.push(xWidth * xSegments / 2); pos.push(0); pos.push(z * zWidth - zSegments * zWidth / 2);
        normals.push(0); normals.push(1); normals.push(0);
    }

    WGLE.geometry.Geometry.call(this, {
        gl: aConfiguration.gl,
        positions: pos,
        //normals: normals,
        drawMode: WebGLRenderingContext.LINES
        //indices: TODO
    });
};

//TODO improve that
//WGLE.geometry.Net.prototype = WGLE.geometry.Geometry.prototype
wglb.extend(WGLE.geometry.Net, WGLE.geometry.Geometry);
