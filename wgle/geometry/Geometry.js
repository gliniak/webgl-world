"use strict";

wglb.prepareNs("WGLE.geometry");

/**
 * Geometry class. Provides vertices attributes like: positions, indices, normals, textureCoordinates, colors and custom (TODO).
 * @param {WebGLRenderingContext} aConfiguration.gl - WebGLRenderingContext object used for creating buffers
 * @param {Number} [aConfiguration.drawMode = WebGLRenderingContext.TRIANGLES] - drawing mode
 * @param {Float32Array or Float64Array or Array} aConfiguration.positions - array of positions of vertices. If it's not typed array then the object will use copy of the passed array transformed to Float32Array
 * @param {Number} aConfiguration.positionsUsage - by default WebGLRenderingContext.STATIC_DRAW (can be also WebGLRenderingContext.DYNAMIC_DRAW or WebGLRenderingContext.STREAM_DRAW)
 * @param {Uint[8|16|32]Array or Int[8|16|32]Array or Array} aConfiguration.indices - optional indices array; if not set drawArrays will be used for drawing (otherwise drawElements). If it's not typed array then it will be transformed to Uint16Array
 * @param {Number} [aConfiguration.indicesUsage = WebGLRenderingContext.STATIC_DRAW]
 * @param {Float32Array or Float64Array or Array} aConfiguration.normals - normal vectors (optional - required for drawing with lighting)
 * @param {Number} [aConfiguration.normalsUsage = WebGLRenderingContext.STATIC_DRAW]
 * @param {Float32Array or Float64Array or Array} aConfiguration.textureCoordinates
 * @param {Number} [aConfiguration.textureCoordinatesUsage = WebGLRenderingContext.STATIC_DRAW]
 * @param {Float32Array or Float64Array or Array} aConfiguration.colors
 * @param {Number} [aConfiguration.colorsUsage = WebGLRenderingContext.STATIC_DRAW]
 * @constructor
 */
WGLE.geometry.Geometry = function(aConfiguration) {
    aConfiguration = aConfiguration || {};

    // WebGLRenderingContext object
    if (!(this.gl = aConfiguration.gl)) throw new Error("WGLE.geometry.Geometry: No WebGLRenderingContext passed");
    if (!(aConfiguration.positions && aConfiguration.positions.length)) throw new Error("WGLE.geometry.Geometry: No positions passed");

    /**
     * OpenGL draw mode - one of following POINTS, LINES, LINE_LOOP, LINE_STRIP, TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN
     */
    this.drawMode = aConfiguration.drawMode === undefined ? WebGLRenderingContext.TRIANGLES : aConfiguration.drawMode;

    /**
     * Array of positions - data will be moved to positionBuffer and the array possible released
     */
    this.positions = [Float32Array, Float64Array].indexOf(aConfiguration.positions.constructor) === -1 ?
            new Float32Array(aConfiguration.positions) : aConfiguration.positions;

    /**
     * see bufferData usage parameter at http://www.khronos.org/registry/webgl/specs/latest/
     */
    this.positionsUsage = aConfiguration.positionsUsage || WebGLRenderingContext.STATIC_DRAW;

    /**
     * Vertex coordinate buffer
     */
    this.positionsBuffer = null;

    // indices (Array of indices (Uint16Array or Int16Array or Int32Array)TODO clarify)
    if (aConfiguration.indices && aConfiguration.indices.length) {
        this.indices = [Int8Array, Int16Array, Int32Array, Uint8Array, Uint16Array, Uint32Array].indexOf(aConfiguration.indices) === -1 ?
                new Uint16Array(aConfiguration.indices) : aConfiguration.indices;
    }
    /**
     * Recommended usage of indices array - see bufferData usage parameter at http://www.khronos.org/registry/webgl/specs/latest/
     */
    this.indicesUsage = aConfiguration.indicesUsage || WebGLRenderingContext.STATIC_DRAW;

    /**
     * Optional buffer of indices
     */
    this.indicesBuffer = null;

    // normal vectors (Float32|64Array of normal vectors (each vertex takes 3 cells) - needed for lighting calculations)
    if (aConfiguration.normals && aConfiguration.normals.length) {
        this.normals = [Float32Array, Float64Array].indexOf(aConfiguration.normals.constructor) === -1 ?
                new Float32Array(aConfiguration.normals) : aConfiguration.normals;
    }
    /**
     * Recommended usage of normal vectors array - see bufferData usage parameter at http://www.khronos.org/registry/webgl/specs/latest/
     */
    this.normalsUsage = aConfiguration.normalsUsage || WebGLRenderingContext.STATIC_DRAW;

    /**
     * Optional buffer of normal vectors
     */
    this.normalsBuffer = null;

    // Float32|64Array of texture coordinates (each vertex takes 2 cells)
    if (aConfiguration.textureCoordinates && aConfiguration.textureCoordinates.length) {
        this.textureCoordinates = [Float32Array, Float64Array].indexOf(aConfiguration.textureCoordinates.constructor) === -1 ?
                new Float32Array(aConfiguration.textureCoordinates) : aConfiguration.textureCoordinates;
    }
    /**
     * Recommended usage of texture coordinates array - see bufferData usage parameter at http://www.khronos.org/registry/webgl/specs/latest/
     */
    this.textureCoordinatesUsage = aConfiguration.textureCoordinatesUsage || WebGLRenderingContext.STATIC_DRAW;

    /**
     * Optional buffer of texture coordinates
     */
    this.textureCoordinatesBuffer = null;

    // colors
    // Float32|64Array of vertices colors (each vertex takes 3 cells) - optional; needed for lighting calculations
    // TODO or 4 cells per vertex?
    if (aConfiguration.colors && aConfiguration.colors.length) {
        this.colors = [Float32Array, Float64Array].indexOf(aConfiguration.colors.constructor) === -1 ?
                new Float32Array(aConfiguration.colors) : aConfiguration.colors;
    }
    /**
     * Recommended usage of colors array - see bufferData usage parameter at http://www.khronos.org/registry/webgl/specs/latest/
     */
    this.colorsUsage = aConfiguration.colorsUsage || WebGLRenderingContext.STATIC_DRAW;

    /**
     * Optional buffer of vertices colors
     */
    this.colorsBuffer = null;

    //TODO custom

    this.loadBuffers();
    //TODO release arrays if already loaded into buffers - maybe use some flag to determine if releasing should be done
    // or add explicit function like 'freeArrays'
};

/**
 * Creates / loads buffers
 */
WGLE.geometry.Geometry.prototype.loadBuffers = function() {
    // Positions buffer
    if (this.positions.length) {
        this.positionsBuffer = this.positionsBuffer || this.gl.createBuffer();
        //this.positionsBuffer.type = this.gl.FLOAT;
        //this.positionsBuffer.size = 3;
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionsBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.positions, this.positionsUsage);
        this.positionsBuffer.numItems = this.positions.length / 3;
    } else console.warn("WGLE.geometry.Geometry.loadBuffers: no positions!");
    WGLE.checkErrors(this.gl, "WGLE.geometry.Geometry: load buffers: positions");

    // Indices buffer
    if (this.indices && this.indices.length) {
        this.indicesBuffer = this.indicesBuffer || this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.indicesBuffer);
        this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, this.indices, this.indicesUsage);
    }
    WGLE.checkErrors(this.gl, "WGLE.geometry.Geometry: load buffers: indices");

    // Texture coordinates buffer
    // TODO add tests for this
    if (this.textureCoordinates && this.textureCoordinates.length) {
        this.textureCoordinatesBuffer = this.textureCoordinatesBuffer || this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.textureCoordinatesBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.textureCoordinates, this.textureCoordinatesUsage);
    }
    WGLE.checkErrors(this.gl, "WGLE.geometry.Geometry: load buffers: texture coordinates");

    // Normal vectors
    // TODO add tests for this
    if (this.normals && this.normals.length) {
        this.normalsBuffer = this.normalsBuffer || this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.normalsBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.normals, this.normalsUsage);
    }
    WGLE.checkErrors(this.gl, "WGLE.geometry.Geometry: load buffers: normal vectors");
};
