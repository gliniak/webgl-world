"use strict";

wglb.prepareNs("WGLE.geometry");

/**
 * @param {Number} aConfiguration.radius - radius of the sphere
 * @param {Number} aConfiguration.latitudeBands
 * @param {Number} aConfiguration.longitudeBands
 * @param {Boolean} aConfiguration.hemisphere - (default false) whether the object should represent hemisphere (not full sphere)
 * @param {Boolean} aConfiguration.outer - (default true) whether the sphere should have triangles created for outer view
 */
WGLE.geometry.Sphere = function(aConfiguration) {
    //this._generate(aConfiguration.radius || 1, aConfiguration.latitudeBands || 10, aConfiguration.longitudeBands || 10)
//};

//WGLE.geometry.Sphere.prototype._generate = function(aRadius, latitudeBands, longitudeBands) {

    var latitudeBands = aConfiguration.latitudeBands || 20;
    var longitudeBands = aConfiguration.longitudeBands || 30;
    var radius = aConfiguration.radius || 1.0;
    var hemisphere = aConfiguration.hemisphere || false;
    var outer = aConfiguration.outer !== false;

    var vertexPositionData = [];
    var normalData = [];
    var textureCoordData = [];
    for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
        var theta = latNumber * Math.PI / latitudeBands;
        hemisphere && (theta /= 2);
        var sinTheta = Math.sin(theta);
        var cosTheta = Math.cos(theta);

        for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
            var phi = longNumber * 2 * Math.PI / longitudeBands;
            var sinPhi = Math.sin(phi);
            var cosPhi = Math.cos(phi);

            var x = cosPhi * sinTheta;
            var y = cosTheta;
            var z = sinPhi * sinTheta;
            var u = 1 - (longNumber / longitudeBands);
            var v = 1 - (latNumber / latitudeBands);

            normalData.push(x);
            normalData.push(y);
            normalData.push(z);
            textureCoordData.push(u);
            textureCoordData.push(v);
            vertexPositionData.push(radius * x);
            vertexPositionData.push(radius * y);
            vertexPositionData.push(radius * z);
        }
    }

    var indexData = [];
    for (var latNumber = 0; latNumber < latitudeBands; latNumber++) {
      for (var longNumber = 0; longNumber < longitudeBands; longNumber++) {
        var first = (latNumber * (longitudeBands + 1)) + longNumber;
        var second = first + longitudeBands + 1;

        if (outer) {
            indexData.push(first + 1);
            indexData.push(second);
            indexData.push(first);

            indexData.push(first + 1);
            indexData.push(second + 1);
            indexData.push(second);
        } else {
            // inverse order
            indexData.push(first);
            indexData.push(second);
            indexData.push(first + 1);

            indexData.push(second);
            indexData.push(second + 1);
            indexData.push(first + 1);
        }
      }
    }

    this.superclass({
        gl: aConfiguration.gl,
        positions: vertexPositionData,
        //normals: normalData,
        //textureCoordinates: textureCoordData, //TODO
        drawMode: WebGLRenderingContext.TRIANGLES,
        //drawMode: WebGLRenderingContext.LINES,
        indices: indexData
    });
};

//console.debug(WGLE.geometry.Sphere.prototype);
wglb.extend(WGLE.geometry.Sphere, WGLE.geometry.Geometry);
//console.debug(WGLE.geometry.Sphere.prototype);
//WGLE.geometry.Sphere.prototype = WGLE.geometry.Geometry.prototype
