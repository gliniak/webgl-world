"use strict";

wglb.prepareNs("WGLE");

/**
 * Animates camera
 * @param {WGLE.Camera} aCamera - camera to animate
 * @constructor
 */
WGLE.CameraAnimation = function(aCamera) {
    if (!(aCamera instanceof WGLE.Camera)) throw new Error("No WGLE.Camera passed");
    this._camera = aCamera;
    this._keyframes = [];
    this._startTime = null;
    this._currIdx = 0;
    this._finishedCallback = null;
    this._loop = false;
    this._dontContinue = false;
};

/**
 * Adds keyframe to the overall animations set.
 * @param {Number} aKeyframe.duration - duration in ms of the transition
 * @param {Array} aKeyframe.targetPosition - final target position (of keyframe)
 * @param {Array} aKeyframe.cameraPosition - final camera position (of keyframe)
 */
WGLE.CameraAnimation.prototype.addKeyframe = function(aKeyframe) {
    if (!this._keyframes.length) {
        this._keyframes.push({
            duration: 0,
            targetPosition: this._camera.target,
            cameraPosition: this._camera.position
        });
    }

    this._keyframes.push(aKeyframe);
};

/**
 * Starts calculations.
 * <p>Note that you must still call apply() to get camera updated in each frame you want see any change
 * @param aLoop - whether to repeat animations set
 */
WGLE.CameraAnimation.prototype.start = function(aLoop, aFinishedCallback) {
    if (!this._keyframes.length) throw new Error("No keyframes added");

    this._loop = aLoop;
    this._finishedCallback = aFinishedCallback;

    this._startTime = new Date().getTime();

    this._dontContinue = false;
    this._currIdx = 0;
    var timeFromStart = 0;
    for (var i = 0; i < this._keyframes.length; i++) {
        var keyframe = this._keyframes[i];
        timeFromStart += keyframe.duration;
        keyframe.durationFromStart = timeFromStart;
        //console.debug("start() idx="+i + " durationFromStart="+timeFromStart);
    }
};

/**
 * Applies camera properties for current frame
 */
WGLE.CameraAnimation.prototype.apply = function() {
    var idx = this._getCurrentIdx();
    if (this._dontContinue) {
        return;
    }

    var prevFrame = this._keyframes[idx];
    var nextFrame = this._keyframes[idx + 1];

    var segmentDuration = ((new Date()) - this._startTime) - prevFrame.durationFromStart;

    var lerpFactor = segmentDuration / nextFrame.duration;
    var newTarget = vec3.create(prevFrame.targetPosition);
    vec3.lerp(prevFrame.targetPosition, nextFrame.targetPosition, lerpFactor, newTarget);
    var newCamPosition = vec3.create(prevFrame.cameraPosition);
    vec3.lerp(prevFrame.cameraPosition, nextFrame.cameraPosition, lerpFactor, newCamPosition);

    this._camera.lookAt(newCamPosition, newTarget);
};

WGLE.CameraAnimation.prototype._getCurrentIdx = function() {
    // start move forward from currently set currIdx
    var durationFromStart = (new Date()) - this._startTime;
    if (durationFromStart <= this._keyframes[this._currIdx + 1].durationFromStart) {
        // no change yet
        return this._currIdx;
    }

    // Next frame!
    this._currIdx++;
    if (this._currIdx === (this._keyframes.length - 1)) {
        // it's last frame -> switch back to first if needed
        this._currIdx = 0;
        this._startTime = new Date().getTime();
        if (!this._loop && this._finishedCallback) {
            // call finished callback if the animation shouldn't be replayed
            this._dontContinue = true;
            this._finishedCallback();
        }
    }

    return this._currIdx;
};
