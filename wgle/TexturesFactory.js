wglb.prepareNs("WGLE");

/**
 * Textures factory. Creates not duplicated (by url) textures.
 * @param {WebGLRenderingContext} aGLContext
 * @constructor
 */
WGLE.TexturesFactory = function(aGLContext) {
    if (!aGLContext) throw new Error("WGLE.TexturesFactory: no webgl context passed");
    this.gl = aGLContext;
    this.urlMap = {};
};

/**
 * Creates new or returns existing (if a texture for given url exists) texture object.
 * Note that passing the same url with different configuration will not create different texture object.
 * The function also adds wgleSourceUrl field to newly created texture and sets its value to input url
 * @param {Number} aConfiguration.magFilter (WebGLRenderingContext.LINEAR by default)
 * @param {Number} aConfiguration.minFilter (WebGLRenderingContext.LINEAR_MIPMAP_LINEAR by default)
 * @param {Number} [aConfiguration.wrapS=WebGLRenderingContext.REPEAT]
 * @param {Number} [aConfiguration.wrapT=WebGLRenderingContext.REPEAT]
 * @return {WebGLTexture} texture object assigned to the given url
 */
WGLE.TexturesFactory.prototype.getTexture = function(aUrl, aConfiguration) {
    if (this.urlMap[aUrl]) return this.urlMap[aUrl];

    aConfiguration = aConfiguration || {};
    var tex = this.urlMap[aUrl] = this.gl.createTexture();
    tex.wgleSourceUrl = aUrl;

    var tempImage = new Image();
    var that = this;
    tempImage.onload = function() {
        var gl = that.gl;
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tempImage);

        // clamp
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, aConfiguration.wrapS || gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, aConfiguration.wrapT || gl.REPEAT);

        // min / mag filters
        var magFilter = aConfiguration.magFilter || gl.LINEAR;
        var minFilter = aConfiguration.minFilter || gl.LINEAR_MIPMAP_LINEAR;

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, magFilter);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, minFilter);

        // TODO: test this, change to mipmaps
        if (minFilter === gl.LINEAR_MIPMAP_NEAREST || minFilter === gl.LINEAR_MIPMAP_LINEAR ||
                minFilter === gl.NEAREST_MIPMAP_NEAREST || minFilter === gl.NEAREST_MIPMAP_LINEAR) {
            gl.hint(gl.GENERATE_MIPMAP_HINT, gl.NICEST);
            gl.generateMipmap(gl.TEXTURE_2D);
        }

        gl.bindTexture(gl.TEXTURE_2D, null);
        tempImage = null;
    };
    tempImage.src = aUrl;

    return tex;
};
