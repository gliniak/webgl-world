"use strict";

wglb.prepareNs("WGLE.program");

/**
 * Class providing (WebGL) program and settings for attributes and uniforms.
 * Bound shaders are able to draw textured and lit object.
 * Note: you should avoid scaling of MVMatrix to avoid artifacts for lighting while using this program
 * @param {WebGLRenderingContext} aConfiguration.renderer - WGLE.Renderer object
 * @param {Array} aConfiguration.fogColor - optional uniform color of the fog ([R, G, B, A]). Values of R,G,B and A is in [0.0, 1.0] set. By default clear color is used
 * @param {Number} [aConfiguration.maxDistance = 500] - distance at which fog (almost) fully covers the object
 * @param {Array} aConfiguraion.ambientColor
 * @param {Array} aConfiguraion.lightColor
 * @param {Array} aConfiguraion.lightPosition - sun position in world coordinates
 * @param {Boolean} aConfiguration.enableSpecular - [true] whether to enable specular
 *
 * @constructor
 */
WGLE.program.PhongProgram = function(aConfiguration) {
    if (!aConfiguration.renderer) throw new Error("WGLE.program.PhongProgram no renderer paramater passed");

    this.fogColor = aConfiguration.fogColor || aConfiguration.renderer.gl.getParameter(WebGLRenderingContext.COLOR_CLEAR_VALUE);
    this.maxDistance = aConfiguration.maxDistance || 500;

    this.lightColor = aConfiguration.lightColor || [1.0, 1.0, 1.0];
    this.ambientColor = aConfiguration.ambientColor || [0.3, 0.3, 0.3];
    this.lightPosition = aConfiguration.lightPosition || [0.0, 10.0, 0.0];

    this.program = aConfiguration.renderer.shadersFactory.getProgram({
        vertexShaderUrl: "wgle/shaders/phong_vert.glsl",
        fragmentShaderUrl: "wgle/shaders/phong_frag.glsl"
    });

    this.uniformsMap = {
        uProjMatrix: { type: "Matrix4fv", value: aConfiguration.renderer.projectionMatrix },
        uMVMatrix: { type: "Matrix4fv", value: "finalMvMatrix" },
        // normal matrix will be extracted from MVMatrix (assuming no scaling was performed)
        //uNormalMatrix: { type: "Matrix4fv", value: "finalMvMatrix" },
        uFogColor: { type: "4fv", value: this.fogColor },
        uMaxDistance: { type: "1f", value: this.maxDistance },
        uSampler: { type: "1i", value: 0 }, // TODO multitextures not supported yet
        uAmbientColor: { type: "3fv", value: this.ambientColor },
        uLightColor: { type: "3fv", value: this.lightColor },
        uLightPos: { type: "3fv", value: this.lightPosition },
        uEnableSpecular: { type: "1i", value: aConfiguration.enableSpecular !== undefined ? aConfiguration.enableSpecular : true }
    };

    this.attributesMap = {
        positionsBuffer: { attributeName: "aPosition" },
        textureCoordinatesBuffer: { attributeName: "aTextureCoord" },
        normalsBuffer: { attributeName: "aVertexNormal" }
    };
};

//TODO use some kind of inheritance
