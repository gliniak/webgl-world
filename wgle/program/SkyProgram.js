"use strict";

wglb.prepareNs("WGLE.program");

/**
 * Class providing (WebGL) program and settings for attributes and uniforms.
 * Bound shaders are able to customize vertices color (only).
 * @param {WebGLRenderingContext} aConfiguration.renderer - WGLE.Renderer object
 * @param {WGLW.World} aConfiguration.world - data model of world
 * @constructor
 */
WGLE.program.SkyProgram = function(aConfiguration) {
    if (!(aConfiguration.renderer instanceof WGLE.Renderer)) throw "WGLE.program.SkyProgram no renderer paramater passed";
    if (!(aConfiguration.world instanceof WGLW.World)) throw "WGLE.program.SkyProgram no world paramater passed";

    this.program = aConfiguration.renderer.shadersFactory.getProgram({
        vertexShaderUrl: "wgle/shaders/sky_vert.glsl",
        fragmentShaderUrl: "wgle/shaders/sky_frag.glsl"
    });

    this._sky = aConfiguration.world.sky;

    this.uniformsMap = {
        uProjMatrix: { type: "Matrix4fv", value: aConfiguration.renderer.projectionMatrix },
        uMVMatrix: { type: "Matrix4fv", value: "finalMvMatrix" },
        uZenithColor: { type: "4fv", value: this._sky.zenithColor },
        uHorizonColor: { type: "4fv", value: this._sky.horizonColor },
        uSunPosition: { type: "3fv", value: /*TODO this._sky.light.sunPosition */[0.0, 0.0, -1.0] },
        uSunColor: { type: "4fv", value: this._sky.light.sunColor },
        uSunGlow: { type: "1f", value: this._sky.light.sunGlow },
        uSunGlowColor: { type: "4fv", value: this._sky.light.sunGlowColor }
    };
    this.attributesMap = { positionsBuffer: { attributeName: "aPosition" }};

    //console.debug("SkyProgram.ctor ", vec3.length(this.uniformsMap.uSunPosition.value), this.uniformsMap.uSunColor.value);

    this._sky.addObserver(this._onSkyChanged.bind(this));
    this._onSkyChanged(this._sky, "");//TODO
};

/**
 * @param {WGLW.Sky} aSkyModel - data model of sky
 * @param {String} aProperty
 */
WGLE.program.SkyProgram.prototype._onSkyChanged = function(aSkyModel, aProperty) {
    //console.debug("._onSkyChanged("+aProperty+")");
    this.uniformsMap.uHorizonColor.value[0] = aSkyModel.horizonColor[0];
    this.uniformsMap.uHorizonColor.value[1] = aSkyModel.horizonColor[1];
    this.uniformsMap.uHorizonColor.value[2] = aSkyModel.horizonColor[2];

    this.uniformsMap.uZenithColor.value[0] = aSkyModel.zenithColor[0];
    this.uniformsMap.uZenithColor.value[1] = aSkyModel.zenithColor[1];
    this.uniformsMap.uZenithColor.value[2] = aSkyModel.zenithColor[2];

    this.uniformsMap.uSunPosition.value[0] = aSkyModel.light.sunPosition[0];
    this.uniformsMap.uSunPosition.value[1] = aSkyModel.light.sunPosition[1];
    this.uniformsMap.uSunPosition.value[2] = aSkyModel.light.sunPosition[2];

    this.uniformsMap.uSunColor.value[0] = aSkyModel.light.sunColor[0];
    this.uniformsMap.uSunColor.value[1] = aSkyModel.light.sunColor[1];
    this.uniformsMap.uSunColor.value[2] = aSkyModel.light.sunColor[2];

    this.uniformsMap.uSunGlow.value = aSkyModel.light.sunGlow;

    //console.debug("SkyProgram._onSkyChanged ", vec3.length(this.uniformsMap.uSunPosition.value), this.uniformsMap.uSunColor.value);
};
