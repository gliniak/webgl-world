"use strict";

wglb.prepareNs("WGLE.program");

/**
 * Class providing (WebGL) program and settings for attributes and uniforms.
 * Bound shaders are able to customize vertices color (only).
 * @param {WebGLRenderingContext} aConfiguration.renderer - WGLE.Renderer object
 * @param {Array} aConfiguration.color - optional uniform color of the vertices ([R, G, B, A]). Values of R,G,B and A is in [0.0, 1.0] set. [0, 1, 0, 1] by default
 * @param {Array} aConfiguration.fogColor - optional uniform color of the fog ([R, G, B, A]). Values of R,G,B and A is in [0.0, 1.0] set. By default clear color is used
 * @param {Number} [aConfiguration.maxDistance = 500] - distance at which fog (almost) fully covers the object
 * @constructor
 */
WGLE.program.CustomColorProgram = function(aConfiguration) {
    if (!aConfiguration.renderer) throw new Error("WGLE.program.CustomColor no renderer paramater passed");

    this.color = aConfiguration.color || [0, 1, 0, 1];
    this.fogColor = aConfiguration.fogColor || aConfiguration.renderer.gl.getParameter(WebGLRenderingContext.COLOR_CLEAR_VALUE);
    this.maxDistance = aConfiguration.maxDistance || 500;

    this.program = aConfiguration.renderer.shadersFactory.getProgram({
        vertexShaderUrl: "wgle/shaders/color_vert.glsl",
        fragmentShaderUrl: "wgle/shaders/color_frag.glsl"
    });

    this.uniformsMap = {
        uColor: { type: "4fv", value: this.color },
        uProjMatrix: { type: "Matrix4fv", value: aConfiguration.renderer.projectionMatrix },
        uMVMatrix: { type: "Matrix4fv", value: "finalMvMatrix" },
        uFogColor: { type: "4fv", value: this.fogColor },
        uMaxDistance: { type: "1f", value: this.maxDistance }
    };
    this.attributesMap = { positionsBuffer: { attributeName: "aPosition" }};
};

//TODO use some kind of inheritance
