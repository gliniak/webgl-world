wglb.prepareNs("WGLE");

/**
 * Textures factory. Creates not duplicated (by url) textures.
 * @param {CanvasElement} aElement
 * @constructor
 */
WGLE.EventsManager = function(aElement) {
    /**
     * List of function called when scroll event (mouse wheel) happend. Argument is bool
     */
    this.scrollEventObservers = [];

    /**
     * List of function called when pan event (mouse wheel) happend. Arguments are: Number, Number
     */
    this.panEventObservers = [];

    /**
     * List of function called every frame. Argument is an object mapping keyboard keys to bool
     */
    this.clickEventObservers = [];

    /**
     * Keys' flags - holds value if the key is pressed
     */
    this._pressed = [];

    this._addKeyboardHandler(aElement);
    this._addMouseHandler(aElement);
};

/**
 * Left key code
 */
WGLE.EventsManager.LEFT_KEY = 37;

/**
 * Right key code
 */
WGLE.EventsManager.RIGHT_KEY = 39;

/**
 * Up key code
 */
WGLE.EventsManager.UP_KEY = 38;

/**
 * Down key code
 */
WGLE.EventsManager.DOWN_KEY = 40;

/**
 * Space key code
 */
WGLE.EventsManager.SPACE_KEY = 32;

/**
 * A key code
 */
WGLE.EventsManager.A_KEY = 65;

/**
 * S key code
 */
WGLE.EventsManager.S_KEY = 83;

/**
 * D key code
 */
WGLE.EventsManager.D_KEY = 68;

/**
 * W key code
 */
WGLE.EventsManager.W_KEY = 87;

/**
 * Q key code
 */
WGLE.EventsManager.Q_KEY = 81;

/**
 * Z key code
 */
WGLE.EventsManager.Z_KEY = 90;

/**
 * Shift key code
 */
WGLE.EventsManager.SHIFT_KEY = 16;

/**
 * Ctrl key code
 */
WGLE.EventsManager.CTRL_KEY = 17;

/**
 * Alt key code
 */
WGLE.EventsManager.ALT_KEY = 18;

/**
 * PageUp key code
 */
WGLE.EventsManager.PAGE_UP = 33;

/**
 * PageDown key code
 */
WGLE.EventsManager.PAGE_DOWN = 34;

/**
 * Calls click observers. Passes array of clicks to these observers
 */
WGLE.EventsManager.prototype.callClickObservers = function() {
    for (var i = 0, len = this.clickEventObservers.length; i < len; ++i) {
        this.clickEventObservers[i](this._pressed);
    }
};

/**
 * Adds event handler for keyboard events. Updates _pressed array
 * @param {CanvasElement} aElement
 */
WGLE.EventsManager.prototype._addKeyboardHandler = function(aElement) {
    var that = this;
    // TODO check other browsers
    document.onkeydown = function(aEvent) { that._pressed[aEvent.keyCode] = true; };
    document.onkeyup = function(aEvent) { that._pressed[aEvent.keyCode] = false; };
};

/**
 * Adds event handler for mouse events
 * @param {CanvasElement} aElement
 */
WGLE.EventsManager.prototype._addMouseHandler = function(aElement) {
    //TODO change it to object's members
    var _lastMouseX = null;
    var _lastMouseY = null;
    var _mouseIsDown = false;

    // mouse wheel
    var scrollUp = wglb.FF ? function(e) { return e.detail < 0; } : function(e) { return e.wheelDelta > 0; };
    var that = this;
    var onScroll = function(aEvent) {
        // call scroll observers
        for (var i = 0, len = that.scrollEventObservers.length; i < len; ++i) {
            that.scrollEventObservers[i](scrollUp(aEvent));
        }
    };
    if (wglb.FF) {
        aElement.addEventListener("DOMMouseScroll", onScroll, false);
    } else {
        aElement.onmousewheel = onScroll;
    }

    // mouse down
    aElement.onmousedown = function(aEvent) {
        _mouseIsDown = true;
        _lastMouseX = aEvent.clientX;
        _lastMouseY = aEvent.clientY;
    };
    // mouse up
    document.onmouseup = function(aEvent){ _mouseIsDown = false; };
    // mouse move
    document.onmousemove = function(aEvent) { //TODO: to sie kreci nawet jak sie zlapie poza canvas
        if (!_mouseIsDown) { return; }
        var x = aEvent.clientX, y = aEvent.clientY;
        //_camera.rotate((_lastMouseX - x) * _mouseSensitivity, (_lastMouseY - y) * _mouseSensitivity);
        // call scroll observers
        for (var i = 0, len = that.panEventObservers.length; i < len; ++i) {
            that.panEventObservers[i](_lastMouseX - x, _lastMouseY - y);
        }
        _lastMouseX = x;
        _lastMouseY = y;
    };
};
