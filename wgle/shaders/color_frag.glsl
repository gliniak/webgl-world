precision highp float;

uniform vec4 uColor;
uniform vec4 uFogColor;
uniform float uMaxDistance;

varying vec4 vPosition;

const float FOG_DENSITY = 2.66;

void main(void) {
    // fog: 1/(e^((x*2.66)^2))
    float normalizedDistance = length(vPosition.xyz) / uMaxDistance;
    float exponent = normalizedDistance * FOG_DENSITY;
    float fogFactor = clamp(1.0 / exp(exponent * exponent), 0.0, 1.0);

    gl_FragColor = mix(uFogColor, uColor, fogFactor);
}
