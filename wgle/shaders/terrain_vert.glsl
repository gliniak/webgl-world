uniform mat4 uMVMatrix;
uniform mat4 uProjMatrix;
uniform vec3 uLightPos;

attribute vec2 aTextureCoord;
attribute vec3 aPosition;
attribute vec3 aVertexNormal;

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;
varying float vAbsoluteHeight;

// W.I.P.
varying vec3 vLightPos;

void main(void) {
    vec4 position = uMVMatrix * vec4(aPosition, 1.0);
    vAbsoluteHeight = aPosition.y;
    gl_Position = uProjMatrix * position;
    vPosition = position.xyz;
    vTextureCoord = aTextureCoord;

    // simplified normal matrix
    vNormal = (uMVMatrix * vec4(aVertexNormal, 0.0)).xyz;

    // W.I.P.
    vLightPos = (uMVMatrix * vec4(uLightPos, 1.0)).xyz;
    //aPosition[3] = 1.0;
    //var posMat = Matrix.create(aPosition).transpose();
    //var ret = _mvMatrix.x(posMat.transpose()).transpose().elements[0];
    //return [ret[0], ret[1], ret[2]];
}
