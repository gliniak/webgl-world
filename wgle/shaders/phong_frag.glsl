precision highp float;

/* fog */
uniform vec4 uFogColor;
uniform float uMaxDistance;
const float FOG_DENSITY = 2.66;

/* light */
uniform vec3 uAmbientColor;
uniform vec3 uLightColor;
uniform sampler2D uSampler;
uniform bool uEnableSpecular;
const vec3 NULL_SPECULAR = vec3(0.0, 0.0, 0.0);

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;

// W.I.P.
varying vec3 vLightPos;

void main(void) {
    vec3 lightDirection = normalize(vLightPos - vPosition);
    float diffuseFactor = max(dot(normalize(vNormal), lightDirection), 0.0);

    // specular
    vec3 specularFactor;
    if (diffuseFactor > 0.0 && uEnableSpecular) {
        vec3 reflectV = normalize(-reflect(lightDirection, vNormal));
        specularFactor = uLightColor * pow(max(dot(reflectV, normalize(-vPosition)), 0.0), 64.0);
    } else {
        specularFactor = NULL_SPECULAR;
    }

    vec3 lightFactor = uAmbientColor + uLightColor * diffuseFactor;
    vec4 texColor = texture2D(uSampler, vTextureCoord);
    vec4 finalColor = vec4(texColor.rgb * lightFactor + specularFactor, texColor.a);

    // fog: 1/(e^((x*2.66)^2))
    float normalizedDistance = length(vPosition) / uMaxDistance;
    float exponent = normalizedDistance * FOG_DENSITY;
    float fogFactor = clamp(1.0 / exp(exponent * exponent), 0.0, 1.0);

    gl_FragColor = mix(uFogColor, finalColor, fogFactor);
}
