uniform mat4 uMVMatrix;
uniform mat4 uProjMatrix;

attribute vec3 aPosition;

varying vec3 vPosition;

void main(void) {
    vPosition = aPosition;
    gl_Position = uProjMatrix * uMVMatrix * vec4(aPosition, 1.0);
}
