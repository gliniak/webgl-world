uniform mat4 uMVMatrix;
uniform mat4 uProjMatrix;

attribute vec3 aPosition;

varying vec4 vPosition;

void main(void) {
    vPosition = uMVMatrix * vec4(aPosition, 1.0);
    gl_Position = uProjMatrix * vPosition;
}
