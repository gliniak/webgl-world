precision highp float;

/* fog */
uniform vec4 uFogColor;
uniform float uMaxDistance;
const float FOG_DENSITY = 2.66;

/* light */
uniform vec3 uAmbientColor;
uniform vec3 uLightColor;
uniform sampler2D uSampler;

/* terrain */
uniform float uWaterLevel;
uniform float uMinHeight;
uniform float uMaxHeight;

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;
varying float vAbsoluteHeight;

// W.I.P.
varying vec3 vLightPos;

void main(void) {
    vec3 lightDirection = normalize(vLightPos - vPosition);
    float diffuseFactor = max(dot(normalize(vNormal), lightDirection), 0.0);

    vec3 lightFactor = uAmbientColor + uLightColor * diffuseFactor;
    vec4 texel = texture2D(uSampler, vTextureCoord);
    vec4 shadedTexel = vec4(texel.rgb * lightFactor, texel.a);

    // fog: 1/(e^((x*2.66)^2))
    float normalizedDistance = length(vPosition) / uMaxDistance;
    float exponent = normalizedDistance * FOG_DENSITY;
    float fogFactor = clamp(1.0 / exp(exponent * exponent), 0.0, 1.0);

    vec4 foggedColor = mix(uFogColor, shadedTexel, fogFactor);

    if (vAbsoluteHeight > uWaterLevel)
    {
        gl_FragColor = foggedColor;
    }
    else
    {
        float waterDepth = (uWaterLevel - vAbsoluteHeight) / (uWaterLevel - uMinHeight);
        gl_FragColor = mix(foggedColor, vec4(0.1, 0.2, 0.5, 1.0), waterDepth);
    }

    //gl_FragColor = vAbsoluteHeight > uWaterLevel ? foggedColor : mix(vec4(0.1, 0.2, 0.5, 1.0), foggedColor, (uWaterLevel - uMinHeight) / (uWaterLevel - vAbsoluteHeight));
}
