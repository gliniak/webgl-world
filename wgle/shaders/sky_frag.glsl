precision highp float;

//const float GLOW_FACTOR = 3.0;
//const float PI = 3.14159265358979323846264;
//const float GLOW_RADIUS = PI / GLOW_FACTOR;
const float SUN_RADIUS = 0.03;
const float SUN_BORDER_THICKNESS = SUN_RADIUS * 0.3;

uniform vec4 uZenithColor;
uniform vec4 uHorizonColor;
uniform vec3 uSunPosition;
uniform vec4 uSunColor;
uniform float uSunGlow;
uniform vec4 uSunGlowColor;

varying vec3 vPosition;

void main(void) {
    float dist = distance(vPosition, uSunPosition);
    if (dist < SUN_RADIUS) {
        gl_FragColor = uSunColor;
    } else {
        float zenithFactor = clamp(vPosition.y, 0.0, 1.0);
       // zenithFactor = clamp(vPosition.y * vPosition.y, 0.5, 1.0);
        //zenithFactor = clamp(1.0 - (0.1/vPosition.y - 1.0), 0.0, 1.0);
//(zenithFactor * zenithFactor * zenithFactor * zenithFactor)*(zenithFactor * zenithFactor * zenithFactor * zenithFactor)
        vec4 color = mix(uHorizonColor, uZenithColor,  zenithFactor);
        float glowFactor = 1.2 * exp(- (-5.0 * uSunGlow + 5.5) * dist * dist);
        color = mix(color, uSunGlowColor, glowFactor);
        gl_FragColor = dist < (SUN_RADIUS + SUN_BORDER_THICKNESS) ? mix(uSunColor, color, (dist - SUN_RADIUS) / SUN_BORDER_THICKNESS) : color;
    }
}
