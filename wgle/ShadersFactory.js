"use strict";

wglb.prepareNs("WGLE");

/**
 * Class responsible for creating shaders and shader programs
 * @param {WebGLContext} aGLContext
 * @constructor
 */
WGLE.ShadersFactory = function(aGLContext) {
    if (aGLContext.constructor !== WebGLRenderingContext) throw new Error("WGLE.ShadersManager no WebGL context passed");
    this.gl = aGLContext;
    this.urlsMap = {};
};

/**
 * Creates or returns existing program containing shaders created from passed files and adds wgleHash property to newly created WebGL program
 * @param {String} aConfiguration.vertexShaderUrl - url of vertex shader source file
 * @param {String} aConfiguration.fragmentShaderUrl - url of fragment shader source file
 * @return {WebGLProgram} WebGL shader program
 * @throws {Error} when there is no passed vertex or fragment shader url, or there were linking problem
 */
WGLE.ShadersFactory.prototype.getProgram = function(aConfiguration) {
    if (!(aConfiguration.vertexShaderUrl && aConfiguration.fragmentShaderUrl))
        throw new Error("WGLE.ShadersFactory.getProgram: missing vertexShaderUrl or fragmentShaderUrl");

    var hash = aConfiguration.vertexShaderUrl + aConfiguration.fragmentShaderUrl;
    if (this.urlsMap[hash]) return this.urlsMap[hash];

    var program = this.gl.createProgram();
    this.urlsMap[hash] = program;
    // hash might be used for sorting (by program) entities
    program.wgleHash = hash;

    this.gl.attachShader(program, this.createShader(aConfiguration.vertexShaderUrl, this.gl.VERTEX_SHADER));
    this.gl.attachShader(program, this.createShader(aConfiguration.fragmentShaderUrl, this.gl.FRAGMENT_SHADER));
    this.gl.linkProgram(program);
    if (!this.gl.getProgramParameter(program, this.gl.LINK_STATUS)) {
        throw new Error("WGLE.ShadersFactory.prototype.getProgram: linking problem");
    }

    return program;
};

/**
 * Creates shader from a given file.
 * @param {String} aUrl - url to shader source file
 * @param {Number} aType - type of shader; either VERTEX_SHADER or FRAGMENT_SHADER
 * @returns {WebGLProgram} new shader instance
 * @throws {Error} when something went wrong during shader compilation
 */
WGLE.ShadersFactory.prototype.createShader = function(aUrl, aType) {
    var shader = this.gl.createShader(aType);
    this.gl.shaderSource(shader, wglb.getContentSynch(aUrl));
    this.gl.compileShader(shader);
    if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
        throw new Error("WGLE.ShadersFactory.createShader from '" + aUrl + "':\n" + this.gl.getShaderInfoLog(shader));
    }
    return shader;
};
