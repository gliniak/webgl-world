"use strict";

wglb.prepareNs("WGLE");

/**
 * Main engine class responsible for managing and displaying objects (so called entities).
 * Sets the following OpenGL states:
 * -enables DEPTH_TEST
 * -enables CULL_FACE
 * -sets clear color to [0, 0, 0, 1]
 * -sets angle of view to 45 degrees
 * -'near' and 'far' distances in the view frustum are 0.1 and 1000 //TODO consider change to [1..1000]
 *
 * @param {HTMLElement} aCanvasOrDiv - if canvas then it's used to display on it. If simple DIV/body element then new canvas will be created
 * @param {Number} aWidth - the width for new canvas if 1st parameter isn't a canvas. Otherwise it doesn't matter
 * @param {Number} aHeight - the height for new canvas if 1st parameter isn't a canvas. Otherwise it doesn't matter
 * @constructor
 * @author [PR]zemek kondor / gliniak / pslawins
 */
WGLE.Renderer = function(aCanvasOrDiv, aWidth, aHeight) {
    if (!aCanvasOrDiv) throw new Error("WGLE.Renderer: No canvas or parent passed");

    if (aCanvasOrDiv.constructor === HTMLCanvasElement) {
        this.canvas = aCanvasOrDiv;
    } else {// DIV or body
        this.canvas = document.createElement("canvas");
        aCanvasOrDiv.appendChild(this.canvas);
    }

    // WebGLRenderingContext object
    this.gl = this.canvas.getContext("experimental-webgl");
    if (!this.gl) throw new Error("WGLE.Renderer: Could not initialise WebGL http://en.wikipedia.org/wiki/WebGL#Support");

    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.enable(this.gl.CULL_FACE);
    this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);

    // Array of drawn entities
    this.entities = [];

    // FPSCounter instance
    this.fpsCounter = new WGLE.FPSCounter();

    // Textures factory
    this.texturesFactory = new WGLE.TexturesFactory(this.gl);

    // Shaders factory
    this.shadersFactory = new WGLE.ShadersFactory(this.gl);

    // Array of functions additionally invoked at the beginning of each frame.
    // User can push his own functions (for example rotating some object)
    this.frameFunctions = [];

    // Projection matrix used by the renderer
    this.projectionMatrix = mat4.create();

    /**
     * Event manager
     */
    this.eventsManager = new WGLE.EventsManager(this.canvas);

    /**
     * Camera of the rendered scene
     */
    this.camera = new WGLE.Camera({ renderer: this });

    // Running flag
    this.running = false;

    // Last depth mask flag value (TODO unit test)
    this._lastDepthMask = true;

    // Last depth test flag value (TODO unit test)
    this._lastDepthTest = true;

    /**
     * Maximum visible distance (far frustum plane)
     */
    this.viewDistance = 1000.0;

    this.setSize(aWidth, aHeight);
    //this.camera.installEventsHandlers(this.eventsManager);

    console.debug("WGLE.Renderer initialized [ " +
            "Version: " + this.gl.getParameter(this.gl.VERSION) + "; " +
            "GLSL version: " + this.gl.getParameter(this.gl.SHADING_LANGUAGE_VERSION) + "; " +
            "Vendor: " + this.gl.getParameter(this.gl.VENDOR) + "; " +
            "Renderer: " + this.gl.getParameter(this.gl.RENDERER) + " ]");
};

/**
 * Sets size of a canvas and updates viewport and perspective
 * @param {Number} aWidth - width of canvas to set
 * @param {Number} aHeight - height of canvas to set
 */
WGLE.Renderer.prototype.setSize = function(aWidth, aHeight) {
    this.canvas.setAttribute("width", aWidth);
    this.canvas.setAttribute("height", aHeight);
    this.gl.viewport(0, 0, aWidth, aHeight);
    // TODO not optimal 1000 / 0.1 is 10000
    this.updatePerspective();
};

/**
 * Requests update of perspective after change in camera field-of-view (fov)
 */
WGLE.Renderer.prototype.updatePerspective = function() {
    mat4.perspective(this.camera.fov, this.canvas.width / this.canvas.height, 0.1, this.viewDistance, this.projectionMatrix);
};

/**
 * Adds 3d object (so called entity) to scene
 */
WGLE.Renderer.prototype.add = function(aEntity) {
    this.entities.push(aEntity);
    //console.warn("WGLE.Renderer.add - sorting disabled due to skydome issue");
    //this.sortEntities();
};

/**
 * Removes 3d object (so called entity) from scene
 */
WGLE.Renderer.prototype.remove = function(aEntity) {
    this.entities.splice(this.entities.indexOf(aEntity), 1);
};

/**
 * Sorts objects array to decrease number of shaders and textures changes
 */
WGLE.Renderer.prototype.sortEntities = function() {
    this.entities.sort(function(a, b) {
        //TODO add texture sorting
        var hashA = a.program.wgleHash, hashB = b.program.wgleHash;
        return hashA < hashB ? -1 : (hashA === hashB ? 0 : 1);
    });
};

/**
 * The frame function
 */
WGLE.Renderer.prototype._frame = function() {
    // handle keyboard - TODO this must be before frameFunctions to center sky-sphere in camera position
    this.eventsManager.callClickObservers();
    // frame functions
    for (var j = 0, len = this.frameFunctions.length; j < len; ++j) this.frameFunctions[j]();
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    this.camera.apply();

    // drawing all entities
    var entity = null;
    var prevEntity = null;
    for (var i = 0, len = this.entities.length; i < len; ++i) {
        prevEntity = entity;
        entity = this.entities[i];

        if (entity.visible) {
            //TODO change texture if needed
            if (this._lastProgram !== entity.program) {
                this.gl.useProgram(entity.program);
                entity.updateAttributesState(prevEntity);
                this._lastProgram = entity.program;
            }
            if (this._lastDepthMask !== entity.depthMask) {
                this.gl.depthMask(entity.depthMask);
                this._lastDepthMask = entity.depthMask;
            }
            if (this._lastDepthTest !== entity.depthTest) {
                this._lastDepthTest = entity.depthTest;
                if (entity.depthTest)
                    this.gl.enable(this.gl.DEPTH_TEST);
                else
                    this.gl.disable(this.gl.DEPTH_TEST);
            }

            //TODO make this update only when camera or object moved
            mat4.multiply(this.camera.mvMatrix, entity.mvMatrix, entity.finalMvMatrix);
            entity.draw();
        }
    }
    WGLE.checkErrors(this.gl, "WGLE.Renderer._frame");

    this.fpsCounter.update();

    this.running && requestAnimationFrame(this._frame.bind(this));
};

/**
 * Starts the drawing loop
 */
WGLE.Renderer.prototype.start = function() {
    if (this.running) return;
    this.running = true;
    requestAnimationFrame(this._frame.bind(this));
};

/**
 * Stops the drawing loop
 */
WGLE.Renderer.prototype.stop = function() {
    this.fpsCounter.reset();
    this.running = false;
};
