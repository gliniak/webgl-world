wglb.prepareNs("WGLE");

/**
 * FPS counter class calculating number of frames per second.
 */
WGLE.FPSCounter = function() {
    // Current FPS value
    this.value = 0;

    // Frequency of updating
    this.frequency = 1000;

    // previous timestamp of update
    this._lastTime = 0;

    // frames counter
    this._counter = 0;

    //  Array of functions invoked when FPS value has changed. New FOS value is passed to each functions.
    // The value isn't changed in every frame but once per number of miliseconds described by frequency field
    this.observers = [];
};

/**
 * Updates internal counters in order to calculate FPS value. Should be invoked in each frame.
 */
WGLE.FPSCounter.prototype.update = function() {
    this._counter++;
    var currTime = new Date().getTime();
    if (currTime - this._lastTime > this.frequency) {
        this.value = Math.round(1000 * this._counter / (currTime - this._lastTime));
        this._lastTime = currTime;
        this._counter = 0;
        for (var i = 0; i < this.observers.length; ++i)
            this.observers[i](this.value);
    }
};

/**
 * Resets counter
 */
WGLE.FPSCounter.prototype.reset = function() {
    this._lastTime = this._counter = this.value = 0;
};
