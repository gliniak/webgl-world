"use strict";

wglb.prepareNs("WGLE");

/**
 * Entity class encapsulating object which can be displayed. The name was chosen (instead of "Object") to avoid mixing with Object-oriented nomenclature.
 * The newly created object is automatically added to the passed renderer. To not display it set visible property to false or use renderer.remove()
 * @param {String} aConfiguration.vertexShaderUrl - url to vertex shader source file
 * @param {String} aConfiguration.fragmentShaderUrl - url to fragment shader source file
 * @param {WebGLProgram} aConfiguration.program - shader program to use - if passed then vertexShaderUrl and fragmentShaderUrl are omitted
 * @param {String} aConfiguration.textureUrl - url to texture image TODO
 * @param {Object} aConfiguration.uniformsMap - maps uniforms names to objects containing type
 * ("type" property - possible values are suffixes to uniform http://www.khronos.org/opengles/sdk/docs/man/xhtml/glUniform.xml function),
 * reference to the value ("value" property) and location (=attribute location which is automatically set by the Entity)
 * @param {Object} aConfiguration.attributesMap - TODO
 * @param {WGLE.geometry.Geometry} aGeometry - geometry providing vertices attributes like positions, normal vectors, texture coordinates etc.
 * @param {WGLE.Renderer} aRenderer - renderer object required to initialize bounded shaders and texture.
 * @constructor
 */
WGLE.Entity = function(aConfiguration, aGeometry, aRenderer, aTextureUrl) {
    if (!aConfiguration) throw new Error("WGLE.Entity: missing configuration parameter");

    // The geometry used by the entity
    if (!(this.geometry = aGeometry)) throw new Error("WGLE.Entity: missing aGeometry parameter");
    if (!aRenderer) throw new Error("WGLE.Entity: missing aRenderer parameter");

    // Shader program used for drawing the object.
    if (aConfiguration.program && aConfiguration.program instanceof WebGLProgram) {
        this.program = aConfiguration.program;
    } else {
        if (!(aConfiguration.vertexShaderUrl && aConfiguration.fragmentShaderUrl))
            throw new Error("WGLE.Entity: missing 'vertexShaderUrl' or 'fragmentShaderUrl' field and no program provided");

        this.program = aRenderer.shadersFactory.getProgram({
            vertexShaderUrl: aConfiguration.vertexShaderUrl,
            fragmentShaderUrl: aConfiguration.fragmentShaderUrl
        });
    }

    /**
     * WebGLRenderingContext instance
     */
    this.gl = aRenderer.gl;

    /**
     * Disabled or enables depth mask when drawing the object. True by default
     */
    this.depthMask = true;

    /**
     * Disables or enables depth test when drawing the object. True by default
     */
    this.depthTest = true;

    /**
     * Object mapping uniforms names to objects holding type and reference to value
     * The type is suffix the same as used in WebGLRenderingContext.uniform* function
     * Note that in current version only array parameters are supported (+ WebGLRenderingContext.uniform1*)
     * @type Object
     * @example
     * {
     *     uColor: { type: "4fv", value: [1.0, 1.0, 0.0, 1.0] },
     *     uProjMatrix: { type: "Matrix4fv", value: renderer.projectionMatrix },
     *     uMVMatrix: { type: "Matrix4fv", value: "mvMatrix" }
     * }
     */
    this.uniformsMap = aConfiguration.uniformsMap;
    this._generateSetUniforms();

    /**
     * Object mapping attributes names to object holding type and reference to value
     * TODO add proper documentation everything is clear
     */
    this.attributesMap = aConfiguration.attributesMap;
    if (!(this.attributesMap && this.attributesMap.positionsBuffer))
        throw new Error("WGLE.Entity: missing attributesMap and/or attributesMap.positionsBuffer");

    /**
     * Holds map attribute location -> enabling state
     */
    this._attributesEnabling = [];

    // find attribute location and enable vertex-attribute arrays
    for (var key in this.attributesMap) {
        var buffer = this.attributesMap[key];
        buffer.location = this.gl.getAttribLocation(this.program, buffer.attributeName);
        if (buffer.location === -1) throw new Error("WGLE.Entity: attribute location for " + buffer.attributeName + " can't be found");
        //if (buffer.enableVertexAttributeArray !== false) this.gl.enableVertexAttribArray(buffer.location);
        this._attributesEnabling[buffer.location] = (buffer.enableVertexAttributeArray !== false);
    }
    WGLE.checkErrors(this.gl, "Entity constructor");

    /**
     * The model-view matrix assigned to object. Defines position, orientation and scale of the entity in world-view.
     */
    this.mvMatrix = mat4.create();
    mat4.identity(this.mvMatrix);

    /**
     * The final model-view matrix which is result of product of the camera and object's models-view matrices.
     * The multiplication is done by the engine in every frame if needed
     * The matrix should not be directly changed by the user.
     * TODO there must be way to update final matrix when user change mvMatrix manually
     */
    this.finalMvMatrix = mat4.create();
    mat4.identity(this.finalMvMatrix);

    /**
     * When false than the object will not be displayed
     * @type Boolean
     */
    this.visible = true;

    /**
     * Texture object assigned to the object
     * @type WebGLTexture
     */
    this.textureObject = null; //TODO unit tests
    //aConfiguration.textureUrl && (this.textureObject = aRenderer.texturesFactory.getTexture(aConfiguration.textureUrl));
    aTextureUrl && (this.textureObject = aRenderer.texturesFactory.getTexture(aTextureUrl));

    // Add the object to the scene
    aRenderer.add(this);
};

/**
 * Updates program's uniforms.
 * Is replaced by faster one generated by _generateSetUniforms
 */
WGLE.Entity.prototype._setUniforms = function() {
    //throw new Exception("Should be overwritten");
    var uniform, val;
    for (var name in this.uniformsMap) {
        uniform = this.uniformsMap[name];
        val = typeof uniform.value === "string" ? this[uniform.value] : uniform.value;
        if (uniform.type.indexOf("Matrix") === -1) {
            this.gl["uniform" + uniform.type](this.gl.getUniformLocation(this.program, name), val);
        } else {
            this.gl["uniform" + uniform.type](this.gl.getUniformLocation(this.program, name), false, val);
        }
    }
};

/**
 * Creates function which sets uniforms and assign it to setUniform property (overrides slow implementation)
 */
WGLE.Entity.prototype._generateSetUniforms = function() {
    var body = "";
    // uColor: { type: "4fv", value: [1.0, 1.0, 0.0, 1.0] }

    // should be series of lines kind of:
    // this.gl.uniform4fv(this.uniformsMap.uColor.location, this.prop); - when uniform.value type is string
    // this.gl.uniform4fv(this._uColorLocation, this.uniformsMap.uColor.value); - when uniform.value is not a string
    // this.gl.uniformMatrixXXXX(this._uColorLocation, this.prop);
    var uniformItem;
    for (var uniformName in this.uniformsMap) {
        uniformItem = this.uniformsMap[uniformName];
        uniformItem.location = this.gl.getUniformLocation(this.program, uniformName);
        if (uniformItem.location === null) throw new Error("WGLE.Entity: uniform location for '" + uniformName + "' not found");

        // proper set uniform function
        body += ("this.gl.uniform" + uniformItem.type + "(");
        // first parameter
        body += ("this.uniformsMap." + uniformName + ".location,");
        // middle parameter if matrix
        uniformItem.type.indexOf("Matrix") !== -1 && (body += "false,");
        // value to set
        body += (typeof uniformItem.value === "string" ? "this." + uniformItem.value + ");" : "this.uniformsMap." + uniformName + ".value);");
    }

    // TODO share this function for object from the same program
    this._setUniforms = eval("(function() {" + body + "})");
    //this._setUniform = new Function(body); - slower than eval
    WGLE.checkErrors(this.gl, "Entity._generateSetUniforms()");
};

/**
 * TODO sort entities to decrease number of those switches
 * TODO unit test this
 * Enables / disables attributes arrays. Should be called when shader program changes (and before drawing)
 * @param {WGLE.Entity} aPrevEntity - previously drawn entity
 */
WGLE.Entity.prototype.updateAttributesState = function(aPrevEntity) {
    //TODO
    var prevArray = aPrevEntity ? aPrevEntity._attributesEnabling : [];
    var prevLength = prevArray.length;
    var currLength = this._attributesEnabling.length;

    var currVal;
    for (var i = 0, len = Math.max(prevLength, currLength); i < len; ++i) {
        currVal = this._attributesEnabling[i];
        if (!prevArray[i] !== !currVal) {
            if (currVal)
                this.gl.enableVertexAttribArray(i);
            else
                this.gl.disableVertexAttribArray(i);
        }
    }
    // TODO remove it after tests
    WGLE.checkErrors(this.gl, "Entity.updateAttributesState()");
};

/**
 * Draws the object
 */
WGLE.Entity.prototype.draw = function() {
    this._setUniforms();
    //TODO set mv matrix also from camera

    //TODO other attributes;
    // Texture coordinates attributes
    if (this.geometry.textureCoordinatesBuffer) {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.geometry.textureCoordinatesBuffer);
        this.gl.vertexAttribPointer(this.attributesMap.textureCoordinatesBuffer.location, 2, this.gl.FLOAT, false, 0, 0);
        this.gl.activeTexture(this.gl.TEXTURE0);
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.textureObject);
        this.gl.uniform1i(this._samplerUniform, 0);
    }

    if (this.geometry.normalsBuffer) {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.geometry.normalsBuffer);
        this.gl.vertexAttribPointer(this.attributesMap.normalsBuffer.location, 3, this.gl.FLOAT, false, 0, 0);
    }

    // Positions and indices attributes
    //this.gl.enableVertexAttribArray(this.attributesMap.positionsBuffer.location);
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.geometry.positionsBuffer);

    this.gl.vertexAttribPointer(this.attributesMap.positionsBuffer.location, 3, this.gl.FLOAT, false, 0, 0);

    if (this.geometry.indicesBuffer) {
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.geometry.indicesBuffer);
        this.gl.drawElements(this.geometry.drawMode, this.geometry.indices.length, this.gl.UNSIGNED_SHORT, 0);
    } else {
        //this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.geometry.positionsBuffer);
        this.gl.drawArrays(this.geometry.drawMode, 0, this.geometry.positionsBuffer.numItems);
    }
};
