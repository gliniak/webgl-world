wglb.prepareNs("WGLE.entities");

/**
 * Entity presenting normal vectors only. You provide vertices positions and normal vectors of original object and
 * the NormalVectorsEntity will produce entity containing lines from original entity vertices.
 * @param {Array} [aConfiguration.color = [1, 0.5, 0, 1]] color of normal vectors
 * @param {Array} aConfiguration.positions - positions of vertices
 * @param {Array} aConfiguration.normals - normal vectors
 * @param {Number} aConfiguraiton.normalsLength - [1] normal vector length
 *
 * @constructor
 */
WGLE.entities.NormalVectorsEntity = function(aConfiguration, aRenderer) {
    if (!aConfiguration.positions) throw new Error("No positions provided");
    if (!aConfiguration.normals) throw new Error("No normal vectors provided");
    if (!(aRenderer instanceof WGLE.Renderer)) throw new Error("No renderer provided");

    var color = aConfiguration.color || [1, 0.5, 0, 1];

    var geometry = new WGLE.geometry.Geometry({
        gl: aRenderer.gl,
        drawMode: WebGLRenderingContext.LINES,
        positions: this._sumOfNormalsAndPositions(aConfiguration.positions,
                aConfiguration.normals,
                aConfiguration.normalsLength || 1)
    });

    this.superclass(new WGLE.program.CustomColorProgram({
        renderer: aRenderer,
        color: color
    }), geometry, aRenderer);
};

wglb.extend(WGLE.entities.NormalVectorsEntity, WGLE.Entity);

WGLE.entities.NormalVectorsEntity.prototype._sumOfNormalsAndPositions = function(aPositions, aNormals, aLength) {
    var ret = [];
    for (var i = 0; i < aPositions.length / 3; ++i) {
        var nr = i * 3;
        ret.push(aPositions[nr]);
        ret.push(aPositions[nr + 1]);
        ret.push(aPositions[nr + 2]);

        ret.push(aPositions[nr] + aNormals[nr] * aLength);
        ret.push(aPositions[nr + 1] + aNormals[nr + 1] * aLength);
        ret.push(aPositions[nr + 2] + aNormals[nr + 2] * aLength);
    }

    return ret;
};


