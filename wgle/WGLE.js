"use strict";

wglb.prepareNs("WGLE");

/**
 * Checks for WebGL errors
 * @param {WebGLRenderingContext} aGl
 * @param {String} aComent - comment added to error message
 * @return {Boolean} true if there were no errors; otherwise false
 */
WGLE.checkErrors = function(aGl, aComment) {
    aComment = aComment || "";
    var err, ret = true;
    while ((err = aGl.getError()) !== aGl.NO_ERROR) {
        ret = false;
        switch (err) {
            case aGl.OUT_OF_MEMORY:
                console.error(aComment + " WGLE.Renderer.checkErrors: OUT_OF_MEMORY");
                break;
            case aGl.INVALID_VALUE:
                console.error(aComment + " WGLE.Renderer.checkErrors: INVALID_VALUE");
                break;
            case aGl.INVALID_FRAMEBUFFER_OPERATION:
                console.error(aComment + " WGLE.Renderer.checkErrors: INVALID_FRAMEBUFFER_OPERATION");
                break;
            case aGl.INVALID_ENUM:
                console.error(aComment + " WGLE.Renderer.checkErrors: INVALID_ENUM");
                break;
            case aGl.INVALID_OPERATION:
                console.error(aComment + " WGLE.Renderer.checkErrors: INVALID_OPERATION");
                break;
            default:
                console.error(aComment + " WGLE.Renderer.checkErrors: other error");
        }
    }
    return ret;
};
