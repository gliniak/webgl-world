"use strict";

wglb.prepareNs("WGLE");

/**
 * Profiler class used to find bottleneck code
 * @constructor
 */
WGLE.Profiler = function() {
    this.markerMap = {};
};

/**
 * marker -> time map. Stores total sums of duration samples.
 */
WGLE.Profiler.prototype.blocksDurations = null;

WGLE.Profiler.prototype.blocksStartTimes = null;

WGLE.Profiler.prototype.blocksStatuses = null;

WGLE.Profiler.prototype.blocksCounter = null;

WGLE.Profiler.prototype.openBlock = function(aId) {

};

WGLE.Profiler.prototype.closeBlock = function(aId) {

};

