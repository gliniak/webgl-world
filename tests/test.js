wglb.prepareNs("WGLE");

/**
 * WebGL Engine test class
 * @param {HTMLElement} aTable - element where messages will be put
 * TODO timers
 */
WGLE.Test = function(aTable) {
    this.div = aTable;
    this.renderer = new WGLE.Renderer(document.body, 100, 20);
    this.texturesFactory = new WGLE.TexturesFactory(this.renderer.gl);
    this.shadersFactory = new WGLE.ShadersFactory(this.renderer.gl);
};

/**
 * Runs unit tests
 */
WGLE.Test.prototype.run = function() {
    console.log("WGLE.Test.run()");

    for (var key in this) {
        if (key.indexOf("test") === 0 && (typeof this[key] === "function")) {
            var row = document.createElement("tr");
            this.div.appendChild(row);
            var nameCell = document.createElement("td");
            nameCell.innerHTML = key;
            nameCell.className = "FuncName";
            row.appendChild(nameCell);

            this._passedCount = 0;
            this._failedCount = 0;
            this._errorString = "";
            this[key]();
            row.className = this._failedCount ? "Failed" : "Passed";
            var resultCell = document.createElement("td");
            resultCell.innerHTML = "Passed: " + this._passedCount + "<br>" + "Failed: " + this._failedCount +
                    "<br><i>" + this._errorString + "</i>";
            row.appendChild(resultCell);
        }
    }
};

/**
 * Compares two values (uses "===" operator)
 */
WGLE.Test.prototype.compare = function(aActual, aExpected, aMessage) {
    if (aActual !== aExpected) {
        this._failedCount++;
        // don't erase first error message
        if (!this._errorString) this._errorString = "Message: " + aMessage + "<br>Actual: " + aActual + "<br>Expected: " + aExpected;
        console.error(aMessage);
    } else
        this._passedCount++;
};

/**
 * Validates expression
 */
WGLE.Test.prototype.validate = function(aVal, aMessage) {
    if (!aVal) {
        this._failedCount++;
        if (!this._errorString) this._errorString = "Message: " + aMessage;
        console.error(aMessage);
    } else
        this._passedCount++;
};

/**
 * Counts own properties of the passed object
 */
WGLE.Test.prototype.countOwnProperties = function(aObject) {
    var ret = 0;
    for (var key in aObject) {
        if (aObject.hasOwnProperty(key))
            ret++;
    }
    return ret;
};

/**
 * Tests Renderer
 */
WGLE.Test.prototype.testRenderer = function() {
    // creation
    var canvasArray = document.getElementsByTagName("canvas");
    var canvas = canvasArray[0];
    this.compare(canvasArray.length, 1, "canvas should be created 1");
    this.compare(canvas, this.renderer.canvas, "canvas should be created 2");
    this.compare(canvas.width, 100, "canvas width is wrong");
    this.compare(canvas.height, 20, "canvas height is wrong");

    var canvas2 = document.createElement("canvas");
    document.body.appendChild(canvas2);
    var renderer2 = new WGLE.Renderer(canvas2);
    this.compare(renderer2.canvas, canvas2, "should be possible to set custom canvas");

    // setSize
    this.renderer.setSize(102, 22);
    this.compare(canvas.width, 102, "canvas width is wrong 2");
    this.compare(canvas.height, 22, "canvas height is wrong 2");

    // entities
    this.compare(this.renderer.entities.length, 0, "entities should be empty");
    var drawCounter = 0;
    var entity = new WGLE.Entity({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment.shdr",
        textureUrl: "assets/piach.jpg",
        attributesMap: { positionsBuffer: { attributeName: "aVertexPosition" }}
    }, new WGLE.geometry.Geometry({
        gl: this.renderer.gl,
        positions: [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0]
    }), this.renderer);
    entity.draw = function(){drawCounter++;};
    //this.renderer.add(entity);// this is done automatically

    this.compare(this.renderer.entities.length, 1, "entities array shouldn't be empty");
    this.renderer.remove(entity);
    this.compare(this.renderer.entities.length, 0, "entities array should be empty 2");

    // fps
    this.validate(this.renderer.fpsCounter instanceof WGLE.FPSCounter, "fpsCounter property missing");

    // projection matrix
    this.compare(this.renderer.projectionMatrix.length, 16, "Projection matrix should be initialized");

    // camera
    this.validate(this.renderer.camera instanceof WGLE.Camera, "Camera should be initialized");

    // textures factory
    this.validate(this.renderer.texturesFactory instanceof WGLE.TexturesFactory, "textures property should be set");

    // shaders factory
    this.validate(this.renderer.shadersFactory instanceof WGLE.ShadersFactory, "shaders property should be set");

    //sort
    //TODO: add tests when ready

    //frame
    //TODO add testing changing program and texture, visible (of entity), depth test and mask
    var frameFunctionCounter = 0;
    this.renderer.frameFunctions.push(function() { frameFunctionCounter++; });
    this.validate(!this.renderer.running, "not running by default");
    this.renderer.add(entity);
    this.renderer.start();
    var that = this;
    window.setTimeout(function() {
        that.validate(frameFunctionCounter > 0, "frame function should be invoked");
        that.validate(drawCounter > 0, "draw function should be invoked");
        that.validate(that.renderer.running, "loop should be running");
        that.renderer.stop();
        that.validate(!that.renderer.running, "loop should be stopped");
    }, 100);
};

/**
 * Tests TextureFactory
 */
WGLE.Test.prototype.testTextureFactory = function() {
    var tex1 = this.texturesFactory.getTexture("assets/piach.jpg", {});
    var gl = this.renderer.gl;
    this.validate(this.texturesFactory.urlMap["assets/piach.jpg"], "map should be filled");
    this.compare(this.texturesFactory.urlMap["assets/piach.jpg"].constructor, WebGLTexture, "texture should be created");
    // need to wait for image loading
    var that = this;
    window.setTimeout(function() {
        that.compare(gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER), WebGLRenderingContext.LINEAR, "Default min filter should be LINEAR");
        that.compare(gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER), WebGLRenderingContext.LINEAR, "Default mag filter should be LINEAR");
    }, 100);

    this.compare(this.countOwnProperties(this.texturesFactory.urlMap), 1, "Should be exactly one entry");
    this.texturesFactory.getTexture("assets/piach2.jpg", {test:"test"});
    this.compare(this.countOwnProperties(this.texturesFactory.urlMap), 2, "Should be exactly 2 entries");
    var tex1bis = this.texturesFactory.getTexture("assets/piach.jpg", {test:"test"});
    this.compare(tex1, tex1bis, "two textures with the same url should be the same object");
    this.compare(this.countOwnProperties(this.texturesFactory.urlMap), 2, "Should be exactly still 2 entries (because of the duplicated url)");
};

/**
 * Tests ShadersFactory
 */
WGLE.Test.prototype.testShadersFactory = function() {
    this.validate(this.shadersFactory.gl instanceof WebGLRenderingContext, "'gl' property should be set");
    this.compare(this.countOwnProperties(this.shadersFactory.urlsMap), 0, "urlsMap should be empty");

    //createShader
    this.compare(this.shadersFactory.createShader("assets/vertex.shdr", this.shadersFactory.gl.VERTEX_SHADER).constructor,
            WebGLShader, "vertex shader should be created");
    this.compare(this.shadersFactory.createShader("assets/fragment.shdr", this.shadersFactory.gl.FRAGMENT_SHADER).constructor,
            WebGLShader, "fragment shader should be created");

    //getProgram
    var program1 = this.shadersFactory.getProgram({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment.shdr"
    });
    this.compare(program1.constructor, WebGLProgram, "Shader program should be created");
    this.compare(this.countOwnProperties(this.shadersFactory.urlsMap), 1, "urlsMap shouldn't be empty");
    this.validate(program1.wgleHash.length > 2, "wgleHash should be set");
    var program2 = this.shadersFactory.getProgram({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment.shdr"
    });
    this.compare(program1, program2, "the same urls should return the same program");
    this.compare(program1.wgleHash, program2.wgleHash, "wgleHashes should be the same");
    this.compare(this.countOwnProperties(this.shadersFactory.urlsMap), 1, "urlsMap should have 1 element still");
    var program3 = this.shadersFactory.getProgram({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment2.shdr"
    });
    this.validate(program1 !== program3, "different url should return new program");
    this.validate(program1.wgleHash !== program3.wgleHash, "wgleHashes shouldn't be the same for different urls");
    this.compare(this.countOwnProperties(this.shadersFactory.urlsMap), 2, "urlsMap should have 2 elements");
};

//TODO FPS counter

//TODO Camera

/**
 * Tests Geometry
 */
WGLE.Test.prototype.testGeometry = function() {
    var gl = this.renderer.gl;
    var pos = [0, 0, 0, 1, 1, 1, 2, 2, 2];
    var ind = [0, 1, 2];

    var geom1 = new WGLE.geometry.Geometry({
        gl: gl,
        positions: pos
    });
    this.compare(geom1.drawMode, WebGLRenderingContext.TRIANGLES, "TRIANGLES should be default drawMode");
    this.validate(geom1.positionsBuffer, "Positions buffer should be created");
    this.compare(geom1.positionsBuffer.numItems, 3, "2 positionsBuffer numItems");
    this.compare(geom1.positionsUsage, gl.STATIC_DRAW, "default positionsUsage");
    this.compare(geom1.positions.constructor, Float32Array, "conversion to Float32Array should be done");
    this.validate(!geom1.indices && !geom1.indicesBuffer, "No indices");

    var geom2 = new WGLE.geometry.Geometry({
        gl: gl,
        drawMode: gl.LINE_STRIP,
        positions: new Float64Array(pos),
        indices: ind,
        positionsUsage: gl.DYNAMIC_DRAW
    });
    this.compare(geom2.drawMode, WebGLRenderingContext.LINE_STRIP, "LINE_STRIP should be custom drawMode");
    this.compare(geom2.indicesUsage, gl.STATIC_DRAW, "default indicesUsage");
    this.compare(geom2.positionsUsage, gl.DYNAMIC_DRAW, "custom positionsUsage");
    this.validate(geom2.indices && geom2.indicesBuffer.constructor === WebGLBuffer, "Indices and its buffer should be created");
    this.compare(geom2.positions.constructor, Float64Array, "Float64Array should be used");
    this.compare(geom2.indices.constructor, Uint16Array, "There should be conversion to Uint16Array");

    //TODO other buffers (color, texCoords, normals)
};

/**
 * Tests Entity
 */
WGLE.Test.prototype.testEntity = function() {
    var entity1 = new WGLE.Entity({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment.shdr",
        attributesMap: { positionsBuffer: { attributeName: "aVertexPosition" }}
    }, new WGLE.geometry.Geometry({
        gl: this.renderer.gl,
        positions: [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0]
    }), this.renderer);
    var entity2 = new WGLE.Entity({
        vertexShaderUrl: "assets/vertex.shdr",
        fragmentShaderUrl: "assets/fragment.shdr",
        attributesMap: { positionsBuffer: { attributeName: "aVertexPosition" }}
    }, new WGLE.geometry.Geometry({
        gl: this.renderer.gl,
        positions: [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0]
    }), this.renderer);

    this.validate(entity1.visible, "Should be visible by default");
    this.validate(entity1.program instanceof WebGLProgram, "program should be initialized");
    this.compare(entity1.program, entity2.program, "programs should be same");

    this.compare(entity1.mvMatrix.length, 16, "mvMatrix should be initialized");

    // passing program instead of urls to shaders
    //var borrowedProgram = entity1.program;
//    var entity3 = new WGLE.Entity({
//        program: borrowedProgram
//    }, );

    //TODO configuration.program
  //TODO rest
};
