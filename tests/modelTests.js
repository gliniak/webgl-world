

WGLE.Test.prototype.testWorld = function() {

    // initialize world
    try {
        new WGLW.World();
        this.validate(false, "should throw errir that there is no data configuration");
    } catch (e) {
        this.validate(true, "");
    }
    try {
        new WGLW.World({});
        this.validate(false, "should throw errir that there is no data provider");
    } catch (e) {
        this.validate(true, "");
    }

    var provider = new WGLW.DataProviderExample();
    var date = new Date();
    var world = new WGLW.World({
        dataProvider: provider,
        date: date
    });
    this.compare(world.dataProvider, provider, "data provider should be set");
    this.compare(world.date, date, "date should be set");
};
