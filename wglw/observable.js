wglb.prepareNs("WGLW");

/**
 * Mixin providing functionality of observable object
 * Adds this functionality to 'this' object.
 * example: WGLW.makeObservable.call(this);
 */
WGLW.makeObservable = function() {
    //console.debug("WGLW.makeObservable", this);

    /**
     * Array of observing functions
     */
    this._observers = [];

    /**
     * Invokes all observer functions
     * @param {Object} aParam
     */
    this.constructor.prototype.notifyObservers = function(aParam) {
        for (var i = 0, len = this._observers.length; i < len; ++i) {
            this._observers[i](this, aParam);
        }
    };

    /**
     * Adds listener function
     * @param aFunction - a function which will be invoked
     */
    this.constructor.prototype.addObserver = function(aFunction) {
        if (typeof aFunction !== "function") throw "addObserver() passed parameter isn't a function";
        this._observers.push(aFunction);
    };

    /**
     * Removes observer from notifying list
     * @param aFunction - a function which won't be invoked anymore
     */
    this.constructor.prototype.removeObserver = function(aFunction) {
        this._observers.splice(this._observers.indexOf(aFunction), 1);
    };
};
