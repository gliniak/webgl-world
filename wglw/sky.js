wglb.prepareNs("WGLW");

/**
 * Sky class holding the sky dome with correct colors and potentially stars texture (and clouds - not in current version).
 *
 * @param {Date} aConfiguration.date - the date to set
 * @param {Number} aConfiguration.sunAngle [Math.PI / 4]
 * @constructor
 */
WGLW.Sky = function (aConfiguration) {
    if (!aConfiguration.date) throw "WGLW.Sky no date provided";

    /**
     * Color of the horizon
     */
    this.horizonColor = [0, 0, 0, 1];

    /**
     * Color of zenith
     */
    this.zenithColor = [0, 0, 0, 1];

    /**
     * Sun path angle
     */
    this.sunAngle = aConfiguration.sunAngle || Math.PI / 4;

    // calculation helpers
    this._sinSunAngle = Math.sin(this.sunAngle);
    this._cosSunAngle = Math.cos(this.sunAngle);

    /**
     * Light (Sun, moon and ambient) properties
     */
    this.light = {
        sunPosition: [],
        sunColor: [0, 0, 0, 1],
        sunGlow: 0.5,
        sunGlowColor: [0, 0, 0, 1],
        ambientColor: [0, 0, 0, 1],
        moonPosition: [],
        moonColor: [0.2, 0.2, 0.2, 1]
    };

    // Add observable functionality
    WGLW.makeObservable.call(this);

    this.setDate(aConfiguration.date);
};

/**
 * @param {Date} aDate
 */
WGLW.Sky.prototype.setDate = function(aDate) {
    //console.debug("WGLW.Sky.setDate(", aDate, ")");
    // sunrise 6:00, sunset 18:00, 23.5 degrees of Earth's axial tilt
    var sunset = 18;

    var SEC_PER_HOUR = 3600;
    var currSecOfHour = aDate.getMinutes() * 60 + aDate.getSeconds();
    // hour expressed in format: 12:30 -> 12.5
    var unifiedHour = aDate.getHours() + currSecOfHour / SEC_PER_HOUR;
    // approximation based on empirical samples
    var blue = Math.max(0, Math.cos(0.03 * (unifiedHour - 12.0) * (unifiedHour - 12.0)));
//console.debug("blue:",blue);
//blue = 50/255;
    //this.zenithColor[0] = 0.3 * blue;
    this.zenithColor[1] = 0.39 * blue;//166
    this.zenithColor[2] = blue;

    this.horizonColor[0] = 0.6 * blue;
    this.horizonColor[1] = 0.93 * blue;
    this.horizonColor[2] = Math.min(1.0, 1.2 * blue);

    // alpha: 0 => midnight, PI => 12.0, 1.5*PI => 18.00
    var sunYAngle = ((unifiedHour / 24.0) * Math.PI * 2.0);// - Math.PI / 2.0;
    //console.debug(unifiedHour, "->", alpha);
    var cosAlpha = Math.cos(sunYAngle);
    this.light.sunPosition[0] = Math.sin(sunYAngle);
    this.light.sunPosition[1] = -cosAlpha * this._sinSunAngle;
    this.light.sunPosition[2] = -cosAlpha * this._cosSunAngle;

    var sunColor = 1.0;
    this.light.sunColor[0] = sunColor;
    this.light.sunColor[1] = sunColor;//Math.max(0, Math.cos((unifiedHour - 11) / 4));
    this.light.sunColor[2] = sunColor;//Math.max(0, Math.cos((unifiedHour - 11) / 4));

    //$\cos \left(\frac{\left(x-18\right)\cdot \pi }{6\cdot 2}\right)$
    // Max sun glow (1.0) at sunset; minimum value is 0.5
    //this.light.sunGlow = 1.0 + -Math.abs(unifiedHour - sunset) / (sunset * 2); // TODO fix this at midnight
    this.light.sunGlow = 0.5 + Math.abs(-Math.abs(unifiedHour - sunset) / 24 + 0.5);
    //TODO sun glow color
    var sunGlowColor = this.light.sunGlowColor;
    sunGlowColor[0] = sunColor;
    sunGlowColor[1] = Math.max(0.5, Math.min(1.0, 2.0 - 0.25 * Math.abs(unifiedHour - 12.0)));
        //sunColor * Math.min(1.0, Math.max(0.1, this.light.sunPosition[1]));
    sunGlowColor[2] = (sunGlowColor[1] * sunGlowColor[1]);
    //console.debug(sunGlowColor, this.light.sunPosition[1]);

    this.light.ambientColor[0] = this.horizonColor[0];
    this.light.ambientColor[1] = this.horizonColor[1];
    this.light.ambientColor[2] = this.horizonColor[2];

    // TODO moon, stars

    // Notify observers
    this.notifyObservers("horizonColor");
    this.notifyObservers("zenithColor");
    this.notifyObservers("light");
};


WGLW.Sky.prototype._setSunColor = function(r, g, b) {
    this.light.sunColor[0] = r;
    this.light.sunColor[1] = g;
    this.light.sunColor[2] = b;
    this.notifyObservers("light");
};

WGLW.Sky.prototype._setHorizonColor = function(r, g, b) {
    this.horizonColor[0] = r;
    this.horizonColor[1] = g;
    this.horizonColor[2] = b;
    this.notifyObservers("light");
};

WGLW.Sky.prototype._setZenithColor = function(r, g, b) {
    this.zenithColor[0] = r;
    this.zenithColor[1] = g;
    this.zenithColor[2] = b;
    this.notifyObservers("light");
};

WGLW.Sky.prototype._setSunGlow = function(g) {
    this.light.sunGlow = g;
    this.notifyObservers("light");
};

WGLW.Sky.prototype._setSunGlowColor = function(r, g, b) {
    this.light.sunGlowColor[0] = r;
    this.light.sunGlowColor[1] = g;
    this.light.sunGlowColor[2] = b;
    this.notifyObservers("light");
};
