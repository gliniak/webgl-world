/*
var wglWorld = {
      date: Date,
      land: {
          terrain: {
            cellSize: 5.0,
            resolutionX: 1024,
            resolutionZ: 1024,
            lengthX: cellSize * resolutionX,
            lengthZ: cellSize * resolutionZ,
            texture: url,
            minHeight: -20.0,
            maxHeight: 30.0,
            vertices: [],
            indices: []
          },
          staticObjects: {}
      }
      environment: {
        fog: {
            color: 4float-Array
        },
        viewDistance: Number,
        weather: Number,
      },
      sky: {
          horizonColor: 4float-Array,
          zenithColor: 4float-Array,
          clouds: ?,
          light: {
              sunPosition: [],
              sunColor: [],
              sunGlow: 0-1,
              sunGlowColor: [],
              ambientColor: [],
              moonPosition: [],
              moonColor: []
          }
      },
      water: {}
};
*/

//====================================================

wglb.prepareNs("WGLW");

/**
 * World class. Manages following modules: terrain, environment, sky, water and objects.
 * Uses WGLW.Observable mixin
 *
 * @param {Object} aConfiguration.dataProvider - DataProvider
 * @param {Number} aConfiguration.cellSize - dimension of single cell. 1.0 by default
 * @param {Number} aConfiguration.minHeight - with maxHeight defines range of possible height. -10.0 by default (sea / water level on 0.0)
 * @param {Number} aConfiguration.maxHeight 20.0 by default
 * @param {Number} [aConfiguration.viewDistance = 500] - max distance view
 * @param {Date} aConfiguration.date - the date. Current version uses only hour part. By default 12:00. Used to compute light (sun) position, color of the sky (in the future maybe night-stars texture)(6:00 AM = sunrise, 6:00 PM = sunset)
 * @param {Number} aConfiguration.weather - cloudiness - 0 = clear (default), 1 = most cloudy (intermediate values allowed - partly clouded)
 * @param {Function} aReadyCallback - the callback function invoked when World has been initialized and every time data has changed (vertices, texture or static objects)
 *
 * @constructor
 */
WGLW.World = function (aConfiguration, aReadyCallback) {
    if (!aConfiguration) throw "WGLW.World: No configuration passed";
    if (!aConfiguration.dataProvider) throw "WGLW.World: No DataProvider";
    this.dataProvider = aConfiguration.dataProvider;
    this._newDataCallback = aReadyCallback || function () {};

    this.ready = false;

    // add observable functionality
    WGLW.makeObservable.call(this);

    /**
     * Used time
     * @type Date
     */
    this.date = aConfiguration.date || new Date(2012, 0, 1, 12, 0, 0, 0);

    /**
     * Terrain data
     * @type WGLW.Terrain
     */
    this.terrain = null;

    /**
     * Static objects like buildings, trees etc.
     * @type Array
     */
    this.staticObjects = null;

    /**
     * Sky data
     * @type WGLW.Sky
     */
    this.sky = new WGLW.Sky({
        date: this.date
    });
    this.sky.addObserver(this._onSkyChanged.bind(this));

    this.environment = new WGLW.Environment({
        viewDistance: aConfiguration.viewDistance || 500,
        weather: aConfiguration.weather === undefined ? 0 : aConfiguration.weather,
        date: this.date
    });
    this.environment.addObserver(this._onEnvironmentChanged.bind(this));
    this.environment.setFogColor(this.sky.horizonColor);

    this.dataProvider.getData(this._getDataHandler.bind(this));
};

/**
 * Updates the indices and vertices (if needed)
 */
WGLW.World.prototype.lookAt = function (aCallback) {
    //TODO consider set callback via separate setter
    aCallback(this.indices);
};

/**
 * Pre-loads fragment of world which will be most probably used soon.
 * Using this function should remove popping effect.
 */
WGLW.World.prototype.prepare = function () {
    //TODO
};

/**
 * Sets daytime of world which may change light conditions, fog
 * @param {Date} aDate - new date
 */
WGLW.World.prototype.setDate = function (aDate) {
    //TODO set fog (more dense in morning) and light
    this.date = aDate;
    this.environment.setDate(aDate);
    this.sky.setDate(aDate);
};

/**
 * Handler for DataProvider.getData
 */
WGLW.World.prototype._getDataHandler = function (aData) {
    this.terrain = new WGLW.Terrain({
        indices: aData.indices,
        normalVectors: aData.normalVectors,
        textureCoordinates: aData.textureCoordinates,
        textureUrl: aData.textureUrl,
        vertices: aData.vertices,

        //TODO not implemented on JsonDataProvider yet
        cellSize: aData.cellSize,
        minHeight: aData.minHeight,
        maxHeight: aData.maxHeight
    });

    this.staticObjects = aData.statics;

    this._newDataCallback();
};

/**
 * Handler for changes in environment object
 * @param {WGLW.World} aSender
 * @param {String} aChangedProperty
 */
WGLW.World.prototype._onEnvironmentChanged = function(aSender, aChangedProperty) {
    this.notifyObservers("environment." + aChangedProperty);
};

/**
 * Handler for changes in sky object
 * @param {WGLW.World} aSender
 * @param {String} aChangedProperty
 */
WGLW.World.prototype._onSkyChanged = function(aSender, aChangedProperty) {
    this.environment.setFogColor(this.sky.horizonColor);
    this.notifyObservers("sky." + aChangedProperty);
};
