wglb.prepareNs("WGLW.provider");

/**
 * Provides height map, terrain texture and static objects (buildings, plants etc)
 * @param aData {Object} the source data
 * @param aData.dataUrl {String} the URL of JSON containing vertices coordinates (positions field) and static objects list (statics field)
 * @param aData.textureUrl {String} [""] the URL of terrain (satellite like) image
 * @constructor
 * @throws
 */
WGLW.provider.JsonDataProvider = function (aData) {
    if (!aData) throw new Error("WGLW.provider.JsonDataProvider: Missing data parameter");
    if (!aData.dataUrl) throw new Error("WGLW.provider.JsonDataProvider: Missing 'dataUrl' field");
    if (!aData.textureUrl) throw new Error("WGLW.provider.JsonDataProvider: Missing 'textureUrl' field");
    this.dataUrl = aData.dataUrl;
    this.textureUrl = aData.textureUrl || "";

    //TODO use workers
};

/**
 * Reads data from the source and passes it as a parameter (1 literal object) of the given callback function.
 * vertices field of the passed argument is a 1-dimension array of vertices coordinates
 * textureUrl field contains URL of the terrain texture
 * statics field contains array of static objects representing objects placed on terrain (trees, buildings etc)
 * indices
 * textureCoordinates
 * normalVectors
 *
 * @param {Function} aCallback - the function invoked when data is read.
 */
WGLW.provider.JsonDataProvider.prototype.getData = function (aCallback) {
    var that = this;
    wglb.getContentAsynch(this.dataUrl, function (jsonText) {
        var obj = JSON.parse(jsonText);
        if (!obj.positions) throw new Error("WGLW.provider.JsonDataProvider.getData(): provided JSON doesn't contain any vertices");
        aCallback({
            vertices: obj.positions,
            normalVectors: obj.normalVectors,
            indices: obj.indices,
            textureCoordinates: obj.textureCoordinates,
            textureUrl: that.textureUrl,
            statics: obj.statics || []
        });
    });
};
