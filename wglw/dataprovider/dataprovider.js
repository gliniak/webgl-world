/**
 * DataProvider is an interface of classes providing following data:
 * -terrain vertices
 * -terrain texture(s)
 * -static objects (plants, buildings etc)
 * WGLW.World class is a user of this class. It's assumed that terrain vertices are delivered as a 1-dimension (flatten) array representing 2-dimensions array.
 * Size of origin (2d) array must be power of 2 - for example 512x1024, 256x256.
 * Vertices are assumed to be normalized (distance between neighbor vertices = 1.0 and z value belongs to [0,1])
 * Due to memory reason DataProvider class should not keep the reference to array data (to allow garbage collector do it's work). It's a WGLW.World responsibility to free used (by vertices and statics) memory.
 * Some examples of possible (not implemented by me) data providers can be: FromDataBaseProvider, FromImageProvider, FromXMLProvider, AutogenerateProvider
 * The DataProvider interface used by WGLW.World is shown below
 */

wglb.prepareNs("WGLW");

/**
 * List of constructor arguments are up to the DataProvider class author and may vary between providers.
 */
WGLW.DataProviderExample = function () {};

/**
 * Requests data delivery and invokes callback function with passed object containing "vertices", "statics" and "texture" fields.
 */
WGLW.DataProviderExample.prototype.getData = function (aCallback) {};
