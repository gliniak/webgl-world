wglb.prepareNs("WGLW");

/**
 * Terrain class.
 *
 * @constructor
 */
WGLW.Terrain = function (aConfiguration) {
    //this.resolutionX: 1024,
    //this.resolutionZ: 1024,
    //this.lengthX: cellSize * resolutionX,
    //this.lengthZ: cellSize * resolutionZ,

    this.vertices = aConfiguration.vertices;
    this.normalVectors = aConfiguration.normalVectors;
    this.textureCoordinates = aConfiguration.textureCoordinates;
    this.textureUrl = aConfiguration.textureUrl;
    this.indices = aConfiguration.indices;

    this.cellSize = aConfiguration.cellSize !== undefined ? aConfiguration.cellSize : this._getCellSize();
    this.maxHeight = aConfiguration.maxHeight;
    this.minHeight = aConfiguration.minHeight;
    if (this.maxHeight === undefined || this.minHeight === undefined) {
        this._updateMinMaxHeight();
    }
};

WGLW.Terrain.prototype._getCellSize = function() {
    return Math.abs(this.vertices[0] - this.vertices[3]);
};

WGLW.Terrain.prototype._updateMinMaxHeight = function() {
    var minHeight = undefined;
    var maxHeight =	undefined;
    var positions = this.vertices;
    for (var i = 0, len = positions.length / 3; i < len; i++) {
        var currentHeight = positions[3 * i + 1];
        if (!minHeight || minHeight > currentHeight) minHeight = currentHeight;
        if (!maxHeight || maxHeight < currentHeight) maxHeight = currentHeight;
    }
    this.minHeight = minHeight;
    this.maxHeight = maxHeight;
};
