wglb.prepareNs("WGLW");

/**
 * Environment class responsible for fog and weather.
 * Fog should depend on daytime.
 * TODO what about latitude & longitude?
 *
 * @param {Number} [aConfiguration.viewDistance = 500] - the distance view
 * @param {Number} aConfiguration.weather - 0 = clear, 1 = most cloudy
 * @param {Date} aConfiguration.date
 * @constructor
 */
WGLW.Environment = function (aConfiguration) {
    //console.debug(aConfiguration)
    if (!(aConfiguration.viewDistance && aConfiguration.weather !== undefined))
        throw new Error("WGLW.Environment: missing viewDistance or weather");

    this.viewDistance = aConfiguration.viewDistance || 500;
    //this.weather = aConfiguration.weather;
    this.fog = {
        color: [0, 0, 0, 1] //TODO
    };

    // add observable functionality
    WGLW.makeObservable.call(this);

    this.setViewDistance(aConfiguration.viewDistance);
    this.setWeather(aConfiguration.weather);
    this.setDate(aConfiguration.date);
};

/**
 * Sets daytime of world which may change light conditions, fog
 * @param {Number} aViewDistance - new view distance
 */
WGLW.Environment.prototype.setViewDistance = function (aViewDistance) {
    //TODO set fog
    this.viewDistance = aViewDistance;
    this.notifyObservers("viewDistance");
};

/**
 * Sets weather condition of world which changes lightning and fog
 * @param {Number} aWeather - new weather condition
 */
WGLW.Environment.prototype.setWeather = function (aWeather) {
    //TODO set fog and light
    this.weather = aWeather;
    this.notifyObservers("weather");
};

/**
 * Updates environment for specific time
 */
WGLW.Environment.prototype.setDate = function (aDate) {
    //TODO set fog
};

/**
 * Sets new value for fog color
 * @param {Array} aColor - source color from values will be copied
 */
WGLW.Environment.prototype.setFogColor = function (aColor) {
    this.fog.color[0] = aColor[0];
    this.fog.color[1] = aColor[1];
    this.fog.color[2] = aColor[2];
    this.fog.color[3] = aColor[3];
};
