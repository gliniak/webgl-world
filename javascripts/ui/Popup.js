"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Creates error popup with text and OK button
 * @constructor
 */
OFP.ui.Popup = function() {
    this._parent = document.getElementById("popupContainer");
};

OFP.ui.Popup.prototype.show = function(aText) {
    var parent = this._parent;
    var path = "assets/ui/popup.html";
    wglb.getContentAsynch(path, function(aContent) {
        if (!aContent) throw new Error("Couldn't load " + path);
        var tempParent = document.createElement("div");
        tempParent.innerHTML = aContent;
        var popupDiv = tempParent.children[0];
        parent.appendChild(popupDiv);
        popupDiv.children[0].innerHTML = aText;
        wglb.showDiv(parent, true);

        popupDiv.children[1].addEventListener("click", function() {
            parent.removeChild(popupDiv);
            wglb.showDiv(parent, false);
        });
    });
};
