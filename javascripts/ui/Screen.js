﻿"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Generic OFP screen - controls cinema borders and its header text, logotype and bottom buttons
 * @constructor
 */
OFP.ui.Screen = function() {
    this._contentRoot = document.getElementById("middleRoot");

    /**
     * Whether showing the screen was requested
     */
    this._showRequested = false;

    /**
     * HTML content of the screen
     */
    this._content = "";

    /**
     * Function which will be called when screen has been shown
     */
    this._showCallback = null;

    this.__buttonsHandlers = [];
};

/**
 * Sets HTML content defined by template file
 * @param aTemplatePath
 */
OFP.ui.Screen.prototype.setContent = function(aTemplatePath) {
    ga("send", "event", "screen", "show", aTemplatePath);
    var that = this;
    wglb.getContentAsynch(aTemplatePath, function(aContent) {
        that._content = aContent;
        if (!aContent) throw new Error("Couldn't load " + aTemplatePath);
        if (that._showRequested) {
            that._contentRoot.innerHTML = aContent;
            that._showCallback && that._showCallback();
        }
    });
};

/**
 * Sets header title and hides logo if text is not empty
 * @param aText - the text to set
 */
OFP.ui.Screen.prototype.setHeaderTitle = function(aText) {
    document.getElementById("logoContainer").style.display = aText ? "none" : null;
    document.getElementById("versionDiv").style.display = aText ? "none" : null;
    document.getElementById("titleHeader").innerHTML = aText;
};

/**
 * Sets buttons texts and callbacks
 * @param {Array} aButtonsConfig - for example [{text: "left button", callback: function() { alert("clicked"); }}]
 */
OFP.ui.Screen.prototype.setButtons = function(aButtonsConfig) {
    if (aButtonsConfig.length !== 3) throw new Error("Button configuration must provide 3 items");

    document.getElementById("bottomButtons").style.display = null;

    // Add buttons listeners
    for (var i = 0; i < 3; i++) {
        var config = aButtonsConfig[i];
        var button = document.getElementById(["leftBottomButton", "middleBottomButton", "rightBottomButton"][i]);
        button.innerHTML = config.text;
        if (config.callback) {
            this.__buttonsHandlers[i] = config.callback;
            button.addEventListener("click", config.callback);
        }
    }
};

OFP.ui.Screen.prototype.cleanButtons = function() {
    // Remove buttons listeners
    for (var i = 0; i < 3; i++) {
        var button = document.getElementById(["leftBottomButton", "middleBottomButton", "rightBottomButton"][i]);
        button.innerHTML = "";
        button.removeEventListener("click", this.__buttonsHandlers[i]);
    }

    document.getElementById("bottomButtons").style.display = "none";
};

/**
 * Shows the screen
 */
OFP.ui.Screen.prototype.show = function() {
    this._showRequested = true;
    if (this._content) {
        this._contentRoot.innerHTML = this._content;
        this._showCallback && this._showCallback();
    }
};

/**
 * Cleans resources up, deregisters listeners etc.
 */
OFP.ui.Screen.prototype.dispose = function() {};
