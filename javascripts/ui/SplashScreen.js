"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Splash screen with fade in/out logotype animations
 * @constructor
 */
OFP.ui.SplashScreen = function() {
    OFP.ui.Screen.call(this);
    // overwrite default parent DIV
    this._contentRoot = document.getElementById("fullScreenContainer");

    this._finishCallback = null;
    this._logoShown = false;

    this.setHeaderTitle(" "); // to immediately hide logo in top bar

    this._showCallback = this._init;
    this.setContent("assets/ui/splash_screen.html");
};

/**
 * Must be synchronized with FadeOut CSS class
 */
OFP.ui.SplashScreen._FADE_TRANSITION_DURATION = 5000;

OFP.ui.SplashScreen._MIN_LOGO_DURATION = OFP.ui.SplashScreen._FADE_TRANSITION_DURATION + 2000;

wglb.extend(OFP.ui.SplashScreen, OFP.ui.Screen);

OFP.ui.SplashScreen.prototype._init = function() {
    this._pushClass(document.getElementById("splashScreenLogoContainer"), " FadeIn");
    window.setTimeout(this._onLogoShown.bind(this), OFP.ui.SplashScreen._MIN_LOGO_DURATION);
};

OFP.ui.SplashScreen.prototype._pushClass = function(aDiv, aClass) {
    // this lines fixes broken animation just after adding new DOM element
    aDiv.style.display = document.defaultView.getComputedStyle(aDiv)['display'];

    if (aDiv._wglbPrevClass) {
        aDiv.className = aDiv._wglbPrevClass + aClass;
    } else {
        aDiv._wglbPrevClass = aDiv.className;
        aDiv.className += aClass;
    }
};

OFP.ui.SplashScreen.prototype.dispose = function() {
    this._contentRoot.innerHTML = null;
    wglb.showDiv(this._contentRoot, false);
};

/**
 * Request to hide splash screen
 * @param aCallback called when fade out is over
 */
OFP.ui.SplashScreen.prototype.finish = function(aCallback) {
    this._finishCallback = aCallback;
    this._logoShown && this._fadeOut();
};

OFP.ui.SplashScreen.prototype._onLogoShown = function() {
    this._logoShown = true;
    this._finishCallback && this._fadeOut();
};

OFP.ui.SplashScreen.prototype._fadeOut = function() {
    this._pushClass(document.getElementById("splashScreenLogoContainer"), " FadeOut");
    window.setTimeout(this._finishCallback, OFP.ui.SplashScreen._FADE_TRANSITION_DURATION + 1000);
};
