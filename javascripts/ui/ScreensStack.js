"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Screens stack
 * @constructor
 */
OFP.ui.ScreensStack = function() {
    this._stack = [];
};

/**
 * Pushes screen on top
 * @param {OFP.ui.Screen} aScreen
 */
OFP.ui.ScreensStack.prototype.push = function(aScreen) {
    this._stack.push(aScreen);
    aScreen.show();
};

OFP.ui.ScreensStack.prototype.pop = function() {
    if (!this.empty()) {
        var popped = this._stack.pop();
        popped.dispose();
        this._stack[this._stack.length - 1].show();
    }
};

/**
 * Whether the stack is empty
 */
OFP.ui.ScreensStack.prototype.empty = function() {
    return !this._stack.length;
};
