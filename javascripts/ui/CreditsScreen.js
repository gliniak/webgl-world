"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Credits screen
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @constructor
 */
OFP.ui.CreditsScreen = function(aScreensStack) {
    OFP.ui.Screen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._showCallback = this._onShown;
    this.setContent("assets/ui/credits.html");
    this._cancelCallback = this._cancel.bind(this);
};

wglb.extend(OFP.ui.CreditsScreen, OFP.ui.Screen);

OFP.ui.CreditsScreen.prototype._onShown = function() {
    this.setHeaderTitle("Credits");
    this.setButtons([
                     { text: "" },
                     { text: "" },
                     { text: "Close", callback: this._cancelCallback }
                     ]);

    var parent = document.getElementById("credits");
    var credits = this._getCredits();
    for (var i = 0, len = credits.length; i < len; ++i) {
        var credit = credits[i];
        var creditDiv = document.createElement("div");
        var roleDiv = document.createElement("div");
        roleDiv.className = "Role";
        roleDiv.innerHTML = credit.role;
        var personDiv = document.createElement("div");
        personDiv.className = "Person";
        personDiv.innerHTML = credit.person;

        creditDiv.appendChild(roleDiv);
        creditDiv.appendChild(personDiv);
        parent.appendChild(creditDiv);
    }
};

OFP.ui.CreditsScreen.prototype._cancel = function() {
    this._screensStack.pop();
};

/**
 * Does the cleanup
 */
OFP.ui.CreditsScreen.prototype.dispose = function() {
    this.cleanButtons();
};

OFP.ui.CreditsScreen.prototype._getCredits = function() {
    return [
            {
                role: "The project based on Operation Flashpoint", person: "by BOHEMIA INTERACTIVE"
            }, {
                role: "AUTHOR", person: "Przemek_kondor"
            }, {
                role: "BUZZ JavaScript HTML5 Audio library", person: "Jay Salvat"
            }, {
                role: "glMatrix JavaScript library", person: "Brandon Jones"
            }, {
                role: "Operation Flashpoint trademark", person: "Codemasters"
            }, {
                role: "Charlie Brown M54 font", person: "justme54s on fontspace.com"
            }, {
                role: "Steelfish font", person: "Ray Larabie"
            }, {
                role: "M113 3d model", person: "Robert Carroll on www.3dvia.com",
            }, {
                role: "head 3d model", person: "twiesner on www.turbosquid.com"
            }, {
                role: "Jeep 3d model", person: "Cristian Chelu on www.turbosquid.com",
            }, {
                role: "Ural 3d model", person: "Roger Ordnance Designs on www.turbosquid.com"
            }, {
                role: "Voice acting", person: "Jacor, Daniel, Trawa & Przemek_kondor"
            }, {
                role: "gamespy replacement", person: "Poweruser on forums.bistudio.com"
            }, {
                role: "hosting", person: "armagame.pl"
            }
    ];
};
