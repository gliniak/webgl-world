"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Single Player missions list OFP screen
 *
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @constructor
 */
OFP.ui.SinglePlayerMissionsScreen = function(aScreensStack) {
    OFP.ui.Screen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._cancelCallback = this._cancel.bind(this);
    this._playCallback = this._play.bind(this);
    this._showCallback = this._initScreen;
    this.setContent("assets/ui/single_player_list.html");
    this._selectedItem = null;
    this._playUrl = null;
    this._error = null;
};

wglb.extend(OFP.ui.SinglePlayerMissionsScreen, OFP.ui.Screen);

/**
 * Initializes SP missions list
 */
OFP.ui.SinglePlayerMissionsScreen.prototype._initScreen = function() {
    this.setHeaderTitle("Single missions");
    this.cleanButtons();
    this.setButtons([
                     { text : "" },
                     { text : "Cancel", callback : this._cancelCallback },
                     { text : "Play", callback : this._playCallback }
                     ]);
    this._addMissions();
};

/**
 * Does the cleanup
 */
OFP.ui.SinglePlayerMissionsScreen.prototype.dispose = function() {
    this.cleanButtons();
};

OFP.ui.SinglePlayerMissionsScreen.prototype._cancel = function() {
    this._screensStack.pop();
};

OFP.ui.SinglePlayerMissionsScreen.prototype._play = function() {
    if (this._playUrl) {
        window.location.href = this._playUrl;
    } else {
        OFP.Game.prototype.showError(this._error);
    }
};

OFP.ui.SinglePlayerMissionsScreen.prototype._getMissions = function() {
    return [
            {
                title : "CTI Everon",
                image : "assets/ui/mission1.jpg",
                description : "There's no place for all of us on Everon. All or nothing. Conquer the whole island or rase the enemy base",
                error : "Couldn't find mfcti116.json under addons/custom"
            },
            {
                title : "C&H Domination PR",
                image : "assets/ui/mission_domination.jpg",
                description : "On this foggy island you can expect nothing but death...",
                error : "Couldn't find editorupdate102.json under addons/custom"
            },
            {
                title : "Race GP Sahrani",
                image : "assets/ui/mission_race_gp_sahrani.jpg",
                description : "<p>Give me fuel, Give me fire, Give me that which I desire!</p><p>by Przemek_kondor, aDaMoS, Trawa & Matte</p>",
                url : "https://www.youtube.com/watch?v=MhCw_YA6jVM"
            },
            {
                title : "DS SABOTAGE",
                image : "assets/ui/mission_ds_sahrani.jpg",
                description : "Another day of blowing up tanks, patrolling, sniping and others. War as usual",
                url : "http://forums.bistudio.com/showthread.php?65644-Coop-12-Dynamic-Sahrani-SABOTAGE"
            },
            {
                title : "Real Tournament 3",
                image : "assets/ui/mission_real_tournament.jpg",
                description : "If you hear the \"bang!\" it's already too late. Make sure it comes from your trigger",
                url : "https://www.youtube.com/watch?v=SUaKV7Bh2Wo"
            } ];
};

OFP.ui.SinglePlayerMissionsScreen.prototype._addMissions = function() {
    var listDiv = document.getElementById("missionsList");
    var missions = this._getMissions();
    for (var i = 0; i < missions.length; i++) {
        // list
        var item = document.createElement("div");
        item.innerHTML = missions[i].title;
        listDiv.appendChild(item);
        var that = this;
        (function(j) {
            item.addEventListener("click", function() {
                that._selectMission(j);
            });
        })(i);

        // description
        this._selectMission(0);
    }
};

OFP.ui.SinglePlayerMissionsScreen.prototype._selectMission = function(index) {
    var mission = this._getMissions()[index];
    document.getElementById("missionDescriptionTitle").innerHTML = mission.title;
    document.getElementById("missionDescriptionImage").src = mission.image;
    document.getElementById("missionDescriptionDescription").innerHTML = mission.description;
    this._playUrl = mission.url;
    this._error = mission.error;

    this._selectedItem && (this._selectedItem.className = "");
    var listDiv = document.getElementById("missionsList");
    this._selectedItem = listDiv.children[index];
    this._selectedItem.className = "Selected";
};
