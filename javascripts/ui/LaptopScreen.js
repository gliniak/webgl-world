﻿"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Generic screen providing animated laptop frame
 * @constructor
 */
OFP.ui.LaptopScreen = function() {
    OFP.ui.Screen.call(this);

    this._showCallback = this._setLaptopContent;

    this.setContent("assets/ui/laptop.html");

    /**
     * @type {String}
     * Must be set in constructor by subclasses
     */
    this._laptopContentPath = "";
};

wglb.extend(OFP.ui.LaptopScreen, OFP.ui.Screen);

/**
 * Sets the content of black laptop screen
 */
OFP.ui.LaptopScreen.prototype._setLaptopContent = function() {
    ga("send", "event", "screen", "show", this._laptopContentPath);
    var that = this;
    wglb.getContentAsynch(this._laptopContentPath, function(aContent) {
        document.getElementById("laptopContainer").innerHTML = aContent;
        that._laptopShowCallback();
    });
};

OFP.ui.LaptopScreen.prototype._laptopShowCallback = function(){};
