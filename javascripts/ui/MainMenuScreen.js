"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Main menu OFP screen
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @param {OFP.Settings} aSettings
 * @param {OFP.Audio} aAudio
 * @constructor
 */
OFP.ui.MainMenuScreen = function(aScreensStack, aSettings, aAudio) {
    OFP.ui.Screen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._settings = aSettings;
    if (!(this._settings instanceof OFP.Settings)) throw new Error("No OFP.Settings passed");

    this._audio = aAudio;
    if (!(this._audio instanceof OFP.Audio)) throw new Error("No OFP.Audio passed");

    this._showCallback = this._initMenu;
    this.setContent("assets/ui/MainMenu.html");

    this._settingsCallback = this._updatePlayerName.bind(this);
    this._playerNameClickCallback = this._openPlayerSettings.bind(this);
    this._singlePlayerClickCallback = this._openSinglePlayerMissions.bind(this);
    this._multiplayerClickCallback = this._openMultiplayer.bind(this);
    this._optionsClickCallback = this._openOptions.bind(this);
};

wglb.extend(OFP.ui.MainMenuScreen, OFP.ui.Screen);

/**
 * Initializes main menu like player text and event listeners
 */
OFP.ui.MainMenuScreen.prototype._initMenu = function() {
    this.setHeaderTitle("");
    this.cleanButtons();
    document.getElementById("menu_player").addEventListener("click", this._playerNameClickCallback);
    document.getElementById("single_mission").addEventListener("click", this._singlePlayerClickCallback);
    document.getElementById("multiplayer").addEventListener("click", this._multiplayerClickCallback);
    document.getElementById("options").addEventListener("click", this._optionsClickCallback);

    // update name when settings are changing
    this._settings.addObserver(this._settingsCallback);
    // and set its initial value
    this._updatePlayerName(null, OFP.Settings.PlayerNameProperty);
};

/**
 * Updated player name text according to appropriate entry in the settings
 * @param {OFP.Settings} aSender
 * @param {String} aProperyName
 */
OFP.ui.MainMenuScreen.prototype._updatePlayerName = function(aSender, aProperyName) {
    var div = document.getElementById("menu_player");
    if (aProperyName === OFP.Settings.PlayerNameProperty && div) {
        div.innerHTML = "PLAYER: " + this._settings.getPlayerName();
    }
};

/**
 * Does the cleanup
 */
OFP.ui.MainMenuScreen.prototype.dispose = function() {
    this._settings.removeObserver(this._settingsCallback);
    document.getElementById("menu_player").removeEventListener("click", this._playerNameClickCallback);
    document.getElementById("single_mission").removeEventListener("click", this._singlePlayerClickCallback);
    document.getElementById("multiplayer").removeEventListener("click", this._multiplayerClickCallback);
    document.getElementById("options").removeEventListener("click", this._optionsClickCallback);
};

/**
 * Opens player settings screen
 */
OFP.ui.MainMenuScreen.prototype._openPlayerSettings = function() {
    this._screensStack.push(new OFP.ui.PlayerSettingsScreen(this._screensStack, this._settings, this._audio));
};

/**
 * Opens SinglePlayer missions screen
 */
OFP.ui.MainMenuScreen.prototype._openSinglePlayerMissions = function() {
    this._screensStack.push(new OFP.ui.SinglePlayerMissionsScreen(this._screensStack));
};

/**
 * Opens MultiPlayer missions screen
 */
OFP.ui.MainMenuScreen.prototype._openMultiplayer = function() {
    this._screensStack.push(new OFP.ui.MultiplayerScreen(this._screensStack));
};

/**
 * Opens options screen
 */
OFP.ui.MainMenuScreen.prototype._openOptions = function() {
    this._screensStack.push(new OFP.ui.OptionsScreen(this._screensStack));
};
