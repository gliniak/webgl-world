"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Screen showing entry options menu to graphic, audio, controls, difficulty & credits
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @constructor
 */
OFP.ui.OptionsScreen = function(aScreensStack) {
    OFP.ui.LaptopScreen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._cancelCallback = this._cancel.bind(this);
    this._creditsCallback = this._openCredits.bind(this);

    this._laptopContentPath = "assets/ui/options.html";
    this._laptopShowCallback = this._initScreen;

    this._videoCallback = this._createErrorFunction("Video options not yet implemented");
    this._audioCallback = this._createErrorFunction("Audio options not yet implemented");
    this._controlsCallback = this._createErrorFunction("controls options not yet implemented");
    this._difficultyCallback = this._createErrorFunction("Difficulty options not yet implemented");
};

wglb.extend(OFP.ui.OptionsScreen, OFP.ui.LaptopScreen);

OFP.ui.OptionsScreen.prototype._initScreen = function() {
    this.setHeaderTitle("Options");
    this.setButtons([
                     { text: "Credits" , callback: this._creditsCallback },
                     { text: "" },
                     { text: "Close", callback: this._cancelCallback }
                     ]);

    document.getElementById("videoOptions").addEventListener("click", this._videoCallback);
    document.getElementById("audioOptions").addEventListener("click", this._audioCallback);
    document.getElementById("controlsOptions").addEventListener("click", this._controlsCallback);
    document.getElementById("difficultyOptions").addEventListener("click", this._difficultyCallback);
};

OFP.ui.OptionsScreen.prototype._cancel = function() {
    this._screensStack.pop();
};

OFP.ui.OptionsScreen.prototype._openCredits = function() {
    this._screensStack.push(new OFP.ui.CreditsScreen(this._screensStack));
};

OFP.ui.OptionsScreen.prototype.dispose = function() {
    this.cleanButtons();

    document.getElementById("videoOptions").removeEventListener("click", this._videoCallback);
    document.getElementById("audioOptions").removeEventListener("click", this._audioCallback);
    document.getElementById("controlsOptions").removeEventListener("click", this._controlsCallback);
    document.getElementById("difficultyOptions").removeEventListener("click", this._difficultyCallback);
};

OFP.ui.OptionsScreen.prototype._createErrorFunction = function(aMessage) {
    return (function() {
        OFP.Game.prototype.showError(aMessage);
    }).bind(this);
};
