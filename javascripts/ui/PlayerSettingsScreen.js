﻿"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Generic screen providing animated laptop frame
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @param {OFP.Settings} aSettings
 * @param {OFP.Audio} aAudio
 * @constructor
 */
OFP.ui.PlayerSettingsScreen = function(aScreensStack, aSettings, aAudio) {
    OFP.ui.LaptopScreen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._settings = aSettings;
    if (!(this._settings instanceof OFP.Settings)) throw new Error("No OFP.Settings passed");

    this._audio = aAudio;
    if (!(this._audio instanceof OFP.Audio)) throw new Error("No OFP.Audio passed");

    this._cancelCallback = this._cancel.bind(this);
    this._saveCallback = this._save.bind(this);

    this.setHeaderTitle("Edit player");
    this.setButtons([
                     { text: "" },
                     { text: "Cancel", callback: this._cancelCallback },
                     { text: "OK", callback: this._saveCallback }
                     ]);
    this._laptopContentPath = "assets/ui/player_settings.html";
    this._laptopShowCallback = this._initSettingsScreen;
};

wglb.extend(OFP.ui.PlayerSettingsScreen, OFP.ui.LaptopScreen);

OFP.ui.PlayerSettingsScreen.prototype._initSettingsScreen = function() {
    document.getElementById("playerNameInput").value = this._settings.getPlayerName();
    document.getElementById("playerId").innerHTML = "Player ID: " + this._settings.getPlayerUID();

    //TODO
    var element = document.getElementById("leftLaptopColumn");
    var style = window.getComputedStyle(element);
    var size = parseInt(style.getPropertyValue("width")) * 0.9;
    var headRenderer = new WGLE.Renderer(document.getElementById("headCanvas"), size, size);
    // that's from css #111 = 17,17,17
    headRenderer.gl.clearColor(17/255.0, 17/255.0, 17/255.0, 1.0);
    headRenderer.start();

    this._head = wglwDemo.EntityFactory.createHead(headRenderer, this._settings.getSelectedFace());
    var headCamera = headRenderer.camera;
    var radius = 1.02;
    headCamera.lookAt([0, 0, radius], [0, 0, 0]);
    // rotVelocity = 1 rotate / 4 seconds = PI/2 per second
    var prevTime = new Date().getTime();
    var angle = 0;

    headRenderer.frameFunctions.push(function() {
      var now = new Date().getTime();
      var timeDiff = now - prevTime;
      prevTime = now;
      var angleDiff = timeDiff * Math.PI / 2000;
      angle = (angle + angleDiff) % (2 * Math.PI);
      headCamera.lookAt([Math.cos(angle) * radius, 0, Math.sin(angle) * radius], headCamera.target, headCamera.up);
    });

    var that = this;
    // faces
    this._facesController = new OFP.ui.SettingSelectController({
        selectElement: document.getElementById("facesSelect"),
        options: this._settings.availableFaces,
        initValue: this._settings.getSelectedFace(),
        callback: function() {
            that._log("face");
            that._head.textureObject = headRenderer.texturesFactory.getTexture(document.getElementById("facesSelect").value);
        }
    });

    // voices
    var sounds = [];
    for (var i = 0; i < this._settings.availableVoices.length; ++i) {
        sounds.push(this._settings.availableVoices[i][1]);
    }
    this._audio.preloadSoundsOgg(sounds);
    this._voicesController = new OFP.ui.SettingSelectController({
        selectElement: document.getElementById("voicesSelect"),
        options: this._settings.availableVoices,
        initValue: this._settings.getSelectedVoice(),
        callback: function() {
            that._log("voice");
            var pitch = document.getElementById("pitchInput").value;
            that._audio.playSound(document.getElementById("voicesSelect").value, pitch);
        }
    });

    // Voice pitch slider
    // TODO extract function from this block:
    var pitchSlider = document.getElementById("pitchInput");
    pitchSlider.min = 0.5;
    pitchSlider.max = 2;
    pitchSlider.step = 0.1;
    pitchSlider.value = this._settings.getVoicePitch();
    this._pitchCallback = function() {
        that._log("pitch");
        var pitch = document.getElementById("pitchInput").value;
        that._audio.playSound(document.getElementById("voicesSelect").value, pitch);
    };
    pitchSlider.addEventListener("change", this._pitchCallback);

    // squad url
    document.getElementById("playerSquadURL").value = this._settings.getSquadUrl();
};

OFP.ui.PlayerSettingsScreen.prototype._save = function() {
    this._settings.setPlayerName(document.getElementById("playerNameInput").value);
    this._settings.setSelectedFace(document.getElementById("facesSelect").value);
    this._settings.setSelectedVoice(document.getElementById("voicesSelect").value);
    this._settings.setVoicePitch(document.getElementById("pitchInput").value);
    this._settings.setSquadUrl(document.getElementById("playerSquadURL").value);

    this._screensStack.pop();
};

OFP.ui.PlayerSettingsScreen.prototype._cancel = function() {
    this._screensStack.pop();
};

OFP.ui.PlayerSettingsScreen.prototype.dispose = function() {
    this.cleanButtons();
    this._facesController.dispose();
    this._voicesController.dispose();
    document.getElementById("pitchInput").removeEventListener("change", this._pitchCallback);
};

OFP.ui.PlayerSettingsScreen.prototype._log = function(aSetting) {
    ga("send", "event", "playerSettingsChanged", aSetting);
};
