"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Populates HTML select element
 * @param {String} aConfiguration.initValue
 * @param {HTMLDiv} aConfiguration.selectElement
 * @param {Array} aConfiguration.options [[string1, value1], [string2, value2], ...]
 * @param {Function} aConfiguration.callback - called when selection changed
 * @constructor
 */
OFP.ui.SettingSelectController = function(aConfiguration) {
    this._selectElement = aConfiguration.selectElement;
    this._options = aConfiguration.options;
    this._changedCallback = aConfiguration.callback;

    this._populate(aConfiguration.initValue);

    this._selectElement.addEventListener("change", this._changedCallback);
};

OFP.ui.SettingSelectController.prototype._populate = function(aInitValue) {
    for (var i = 0; i < this._options.length; i++) {
        var options = this._options[i];
        var el = document.createElement("option");
        el.textContent = options[0];
        el.value = options[1];
        this._selectElement.appendChild(el);
    }

    this._selectElement.value = aInitValue;
};

/**
* Cleans up.
*/
OFP.ui.SettingSelectController.prototype.dispose = function() {
    this._selectElement.removeEventListener("change", this._changedCallback);
};
