"use strict";

wglb.prepareNs("OFP.ui");

/**
 * Screen showing MP Servers list
 * @param {OFP.ui.ScreensStack} aScreensStack
 * @constructor
 */
OFP.ui.MultiplayerScreen = function(aScreensStack) {
    OFP.ui.LaptopScreen.call(this);

    this._screensStack = aScreensStack;
    if (!(this._screensStack instanceof OFP.ui.ScreensStack)) throw new Error("No OFP.ui.ScreensStack passed");

    this._mpConnection = new OFP.MultiplayerConnection();

    this._cancelCallback = this._cancel.bind(this);
    this._fetchServersCallback = this._handleServersList.bind(this);

    this._laptopContentPath = "assets/ui/multiplayer.html";
    this._laptopShowCallback = this._initScreen;

    this._selectedRow = null;
};

wglb.extend(OFP.ui.MultiplayerScreen, OFP.ui.LaptopScreen);

OFP.ui.MultiplayerScreen.prototype._initScreen = function() {
    this.setHeaderTitle("Network games on Internet");
    this.setButtons([
                     { text: "" , callback: null },
                     { text: "Cancel" , callback: this._cancelCallback },
                     { text: "Join", callback: this._createErrorFunction("Joining servers not implemented") }
                     ]);

    this._mpTable = document.getElementById("mp_servers_table");
    this._bottombarSlot = document.getElementById("bottombar_left_slot");

    this._createProgressbar();
    this._mpConnection.fetchServersList(this._fetchServersCallback);
};

OFP.ui.MultiplayerScreen.prototype._cancel = function() {
    this._screensStack.pop();
};

OFP.ui.MultiplayerScreen.prototype.dispose = function() {
    this.cleanButtons();
    this._removeProgressbar();
};

OFP.ui.MultiplayerScreen.prototype._createProgressbar = function() {
    var div = document.createElement("div");
    div.className = "MultiplayerProgressbarWrapper";

    var logo = document.createElement("img");
    logo.src = "assets/powerserver.png";
    div.appendChild(logo);

    this._progressbar = document.createElement("progress");
    this._progressbar.className = "MultiplayerProgressbar";
    div.appendChild(this._progressbar);

    this._bottombarSlot.appendChild(div);
};

OFP.ui.MultiplayerScreen.prototype._removeProgressbar = function() {
    this._bottombarSlot.innerHTML = "";
};

OFP.ui.MultiplayerScreen.prototype._handleServersList = function(aError, aServersIp) {
   // this._clearTable();
    if (aError) {
        console.error("Couldn't fetch servers: " + aError);
        return;
    }

    this._progressbar.value = 0;
    this._progressbar.max = aServersIp.length;

    var responseCounter = 0;
    var that = this;
    for (var i = 0, len = aServersIp.length; i < len; i++) {
        this._mpConnection.fetchServerDetails(aServersIp[i], function(aDetailsError, aServer) {
            that._progressbar.value = ++responseCounter;
            console.debug(aDetailsError, aServer);
            if (!aDetailsError) {
                that._addServer(aServer);
            } else {
                console.error(aDetailsError);
            }
        });
    }
};

OFP.ui.MultiplayerScreen.prototype._clearTable = function() {
    var header = document.getElementById("mp_table_header");
    while (this._mpTable.lastChild && this._mpTable.lastChild !== header) {
        this._mpTable.removeChild(this._mpTable.lastChild);
    }
};

OFP.ui.MultiplayerScreen.prototype._populateTable = function(aServers) {
    this._clearTable();
    for (var i = 0; i < aServers.length; i++) {
        this._addServer(aServers[i]);
    }
};

/**
 * Adds server row to the table
 * @param {OFP.MultiplayerServer} aServer
 */
OFP.ui.MultiplayerScreen.prototype._addServer = function(aServer) {
    var row = document.createElement("tr");
    this._addTableCell(row, aServer.name + "<br>" + "Version: " + aServer.version + " Required: " + aServer.requiredVersion);
    this._addTableCell(row, (aServer.mission || " ") + "<br>" + aServer.mod);
    this._addTableCell(row, aServer.gameStatus, this._isGamestatusRed(aServer.gameStatus));
    this._addTableCell(row, aServer.playerCount + "/" + aServer.maxPlayers);
    this._addTableCell(row, aServer.ping);

    var that = this;
    row.addEventListener("click", function() {
        that._selectRow(row);
    });

    this._mpTable.appendChild(row);
};

OFP.ui.MultiplayerScreen.prototype._selectRow = function(aRow) {
    if (this._selectedRow) {
        this._selectedRow.className = undefined;
    }

    this._selectedRow = aRow;
    this._selectedRow.className = "Selected";
};

OFP.ui.MultiplayerScreen.prototype._addTableCell = function(aRow, aText, aRed) {
    var cell = document.createElement("td");
    if (aRed) cell.className = "Red";
    cell.innerHTML = aText;
    aRow.appendChild(cell);
};

OFP.ui.MultiplayerScreen.prototype._isGamestatusRed = function(aGamestatus) {
    return (["Playing", "Debriefing", "Briefing"].indexOf(aGamestatus) >= 0);
};

OFP.ui.MultiplayerScreen.prototype._createErrorFunction = function(aMessage) {
    return (function() {
        OFP.Game.prototype.showError(aMessage);
    }).bind(this);
};
