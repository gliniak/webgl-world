wglb.prepareNs("OFP");

/**
 * Manages music tracks and other sounds
 * @param {OFP.Settings} aSettings - OFP.Settings object
 * @constructor
 */
OFP.Audio = function(aSettings) {
    if (!(aSettings instanceof OFP.Settings)) throw new Error("No settings passed");

    this.introTrack = null;
    this._settings = aSettings;

    this._preloadIntro();
    this._preloaded = {};
    this._currentSound = null;

    aSettings.addObserver(this._onSettingsChanged.bind(this));
};

/**
 * Preloads audio files which are required at the beginning (like intro track)
 */
OFP.Audio.prototype._preloadIntro = function() {
    var introTracks = ["06_dark_side", "08_ocean", "12_casualties_of_war"];
    var track = introTracks[Math.floor(Math.random() * introTracks.length)];

    this.introTrack = new buzz.sound("assets/audio/music/" + track, {
        formats: ["ogg"/*, "mp3"*/], // TODO support html5 browsers
        preload: true,
        loop: true
    });
    this.introTrack.setVolume(100 * this._settings.getMusicVolume());
};

OFP.Audio.prototype.preloadSoundsOgg = function(aSounds) {
    for (var i = 0; i < aSounds.length; ++i) {
        var soundUrl = aSounds[i];
        this._preloaded[soundUrl] = this._preloaded[soundUrl] || new buzz.sound(soundUrl, {
            preload: true,
            loop: false
        });
    }
};

/**
 * Plays preloaded sound specified by url
 * @param {String} aSoundUrl
 * @param {Number} aPitch - sound speed [1]
 */
OFP.Audio.prototype.playSound = function(aSoundUrl, aPitch) {
    this._currentSound && !this._currentSound.isEnded() && this._currentSound.stop();
    var sound = this._preloaded[aSoundUrl];
    if (!sound) throw new Error(aSoundUrl +" was not preloaded");
    sound.setVolume(100 * this._settings.getSoundVolume()).setSpeed(aPitch || 1).play();
    this._currentSound = sound;
};

/**
 * Updates music and sounds according to changes in settings
 * @param {OFP.Settings} aSettings
 * @param {String} aProperty - updated property
 */
OFP.Audio.prototype._onSettingsChanged = function(aSettings, aProperty) {
    if (aProperty === OFP.Settings.MusicVolumeProperty) {
        this.introTrack.setVolume(100 * this._settings.getMusicVolume());
    }
};

/**
 * Whether audio (playing sound & music) is supported
 */
OFP.Audio.prototype.isSupported = function() {
    return buzz.isSupported() && buzz.isOGGSupported();
};
