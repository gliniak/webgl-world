"use strict";

wglb.prepareNs("wglwDemo");

/**
 * The demo application object
 */
wglwDemo.app = {
    worldModel: null,
    dataProvider: null,
    renderer: null,
    worldView: null,

    start: function () {
        //!Detector.webgl && Detector.addGetWebGLMessage();

        this.dataProvider = new WGLW.provider.JsonDataProvider({
            dataUrl: "assets/models3d/world/intro/desert_island.json",
            textureUrl: "assets/models3d/world/intro/sand.jpg"
        });
        var that = this;
        this.worldModel = new WGLW.World({
            dataProvider: this.dataProvider,
            viewDistance: 3200,
        }, function () {
            // create objects
            wglwDemo.EntityFactory.createSkyBox(that.renderer, that.worldModel);
            wglwDemo.EntityFactory.createTestObjects(that.renderer, that.worldModel);
            wglwDemo.EntityFactory.createTerrain(that.renderer, that.worldModel);
            wglwDemo.EntityFactory.createOcean(that.renderer, that.worldModel);
        });

        this._initializeRenderer();

        wglwDemo.EntityFactory.showFPS(this.renderer);
    },

    startWloclawek: function() {
        //!Detector.webgl && Detector.addGetWebGLMessage();

        this.dataProvider = new WGLW.provider.JsonDataProvider({
            dataUrl: "assets/models3d/world/wloclawek/wloclawek.json",
            textureUrl: "assets/models3d/world/wloclawek/green.png"
        });
        var that = this;
        this.worldModel = new WGLW.World({
            dataProvider: this.dataProvider,
            viewDistance: 3200,
        }, function () {
            // create objects
            wglwDemo.EntityFactory.createSkyBox(that.renderer, that.worldModel);
            wglwDemo.EntityFactory.createTerrain(that.renderer, that.worldModel);
            // water must be the last one to draw to have blending working properly
            wglwDemo.EntityFactory.createOcean(that.renderer, that.worldModel);
        });

        this._initializeRenderer();

        wglwDemo.EntityFactory.showFPS(this.renderer);
    },

    _initializeRenderer: function() {
        this.renderer = new WGLE.Renderer(document.getElementById("webglCanvas"), window.innerWidth, window.innerHeight * 0.9);
        this.renderer.viewDistance = this.worldModel.environment.viewDistance;
        this.renderer.updatePerspective();
        var gl = this.renderer.gl;
        gl.enable(gl.BLEND);

        var r = this.renderer;
        window.onresize = function(aEvent) {
            r.setSize(aEvent.target.innerWidth, aEvent.target.innerHeight * 0.9);
        };

        this.renderer.start();
    }
};
