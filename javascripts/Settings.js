"use strict";

wglb.prepareNs("OFP");

/**
 * Holds game, player, video etc. settings
 */
OFP.Settings = function() {

    // if local storage not available - at least for the session keep settings - create dummy storage
    this._storage = {};

    /**
     * Whether the settings are persistent between sessions.
     */
    this.arePersistent = function() {
        if (typeof(Storage) === "undefined") {
            return false;
        }

        try {
            window.localStorage;
        } catch(e) {
            return false;
        }

        return true;
    };

    /**
     * Gets setting value
     */
    this._getProperty = function(aProperty, aFallbackValue) {
        var ret = this._storage[aProperty];
        return ret === undefined ? aFallbackValue : ret;
    };

    /**
     * Sets settings value
     */
    this._setProperty = function(aProperty, aValue) {
        this._storage[aProperty] = aValue;
        this.notifyObservers(aProperty);
    };

    /**
     * Gets player name
     * @returns {String}
     */
    this.getPlayerName = function() {
        return this._getProperty(OFP.Settings.PlayerNameProperty, "PLAYER 1");
    };

    /**
     * Sets player name
     * @param {String} aPlayerName
     */
    this.setPlayerName = function(aPlayerName) {
        this._setProperty(OFP.Settings.PlayerNameProperty, aPlayerName);
    };

    /**
     * Array containing 2 elements arrays [name, face url]
     * @type {Array}
     */
    this.availableFaces = [
        ["Adam S.", "assets/models3d/human/ofp_head_1.jpg"],
        ["Greg M.", "assets/models3d/human/ofp_head_2.jpg"],
        ["Matt L.", "assets/models3d/human/ofp_head_3.jpg"],
    ];

    /**
     * Gets selected face from availableFaces
     * @returns {String} uri of texture file
     */
    this.getSelectedFace = function() {
        return this._getProperty(OFP.Settings.PlayerFaceProperty, this.availableFaces[0][1]);
    };

    /**
     * Sets selected face from availableFaces
     * @param {String} aFaceUri
     */
    this.setSelectedFace = function(aFaceUri) {
        this._setProperty(OFP.Settings.PlayerFaceProperty, aFaceUri);
    };

    /**
     * Array containing 2 elements arrays [name, voice url]
     */
    this.availableVoices = [
        ["Jacor", "assets/audio/sound/przegiecie_paly.ogg"],
        ["Daniel", "assets/audio/sound/prr.ogg"],
        ["Trawa", "assets/audio/sound/nie_dam.ogg"],
        ["Przemek", "assets/audio/sound/isayagain.ogg"],
    ];

    /**
     * Gets selected voice from availableVoices
     * @returns {String}
     */
    this.getSelectedVoice = function() {
        return this._getProperty(OFP.Settings.PlayerVoiceProperty, this.availableVoices[0][1]);
    };

    /**
     * Sets selected face from availableVoices
     * @param {String} aVoiceUri
     */
    this.setSelectedVoice = function(aVoiceUri) {
        this._setProperty(OFP.Settings.PlayerVoiceProperty, aVoiceUri);
    };

    /**
     * Sets view distance
     * @param {Number} aDistance - max view distance
     */
    this.setViewDistance = function(aDistance) {
        this._setProperty(OFP.Settings.ViewDistanceProperty, aDistance);
    };

    /**
     * Gets stored view distance
     * @returns {Number} view distance in meters
     */
    this.getViewDistance = function() {
        var retString = this._getProperty(OFP.Settings.ViewDistanceProperty, 3200);
        return parseFloat(retString);
    };

    /**
     * Gets music tracks volume
     * @returns {Number} from [0..1] range
     */
    this.getMusicVolume = function() {
        var retString = this._getProperty(OFP.Settings.MusicVolumeProperty, 0.3);
        return parseFloat(retString);
    };

    /**
     * Sets music tracks volume
     * @param {Number} aVolume - number from [0..1] range.
     */
    this.setMusicVolume = function(aVolume) {
        this._setProperty(OFP.Settings.MusicVolumeProperty, Math.min(1, Math.max(0, aVolume)));
    };

    /**
     * Gets sounds volume
     * @returns {Number} from [0..1] range
     */
    this.getSoundVolume = function() {
        var retString = this._getProperty(OFP.Settings.SoundVolumeProperty, 0.3);
        return parseFloat(retString);
    };

    /**
     * Sets sounds volume
     * @param {Number} aVolume - number from [0..1] range.
     */
    this.setSoundVolume = function(aVolume) {
        this._setProperty(OFP.Settings.SoundVolumeProperty, Math.min(1, Math.max(0, aVolume)));
    };

    /**
     * Gets voice pitch
     * @returns {Number} from [0.5..2] range (percentage of speed)
     */
    this.getVoicePitch = function() {
        var retString = this._getProperty(OFP.Settings.VoicePitchProperty, 1);
        return parseFloat(retString);
    };

    /**
     * Sets voice pitch
     * @param {Number} aPitch - number from [0.5..2] range.
     */
    this.setVoicePitch = function(aPitch) {
        this._setProperty(OFP.Settings.VoicePitchProperty, Math.min(2, Math.max(0.5, aPitch)));
    };

    /**
     * Gets squad url
     * @returns {String} squad url
     */
    this.getSquadUrl = function() {
        return this._getProperty(OFP.Settings.SquadUrlProperty, "");
    };

    /**
     * Sets squad url
     * @param {String} aSquadUrl
     */
    this.setSquadUrl = function(aSquadUrl) {
        this._setProperty(OFP.Settings.SquadUrlProperty, aSquadUrl);
    };

    /**
     * Gets player's UID
     */
    this.getPlayerUID = function() {
        var uid = this._getProperty(OFP.Settings.PlayerUID, "");
        if (!uid) {
            var newUID = Math.ceil(Math.random() * 100000000);
            this._setProperty(OFP.Settings.PlayerUID, newUID);
            uid = newUID;
        }
        return uid;
    };

    // makes the class observable
    WGLW.makeObservable.call(this);

    if (this.arePersistent()) {
        this._storage = window.localStorage;
    }
};

/**
 * Name of local storage property holding player's name
 */
OFP.Settings.PlayerNameProperty = "PlayerName";

/**
 * Name of local storage property holding player's face
 */
OFP.Settings.PlayerFaceProperty = "PlayerFace";

/**
 * Name of local storage property holding player's voice
 */
OFP.Settings.PlayerVoiceProperty = "PlayerVoice";

/**
 * Name of local storage property holding view distance
 */
OFP.Settings.ViewDistanceProperty = "ViewDistance";

/**
 * Name of local storage property holding music volume
 */
OFP.Settings.MusicVolumeProperty = "MusicVolume";

/**
 * Name of local storage property holding sounds volume
 */
OFP.Settings.SoundVolumeProperty = "SoundVolume";

/**
 * Name of local storage property holding voice pitch
 */
OFP.Settings.VoicePitchProperty = "VoicePitch";

/**
 * Name of local storage property holding player's UID
 */
OFP.Settings.PlayerUID = "PlayerUID";

/**
 * Name of local storage property holding squad url
 */
OFP.Settings.SquadUrlProperty = "SquadUrl";
