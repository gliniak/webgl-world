"use strict";

wglb.prepareNs("OFP");

/**
 * Game object - holds and manages the settings, audio, renderer etc.
 * @constructor
 */
OFP.Game = function() {

    /**
     * Holds game settings
     */
    this._settings = new OFP.Settings();

    /**
     * Holds audio manager
     */
    this._audio = new OFP.Audio(this._settings);

    /**
     * The WGLE.Renderer
     */
    this._renderer = null;

    this._dataProvider = null;

    /**
     * The world data
     */
    this._worldModel = null;

    /**
     * Stack of screens
     */
    this._screensStack = new OFP.ui.ScreensStack();

    /**
     * @type {OFP.ui.SplashScreen}
     */
    this._splashScreen = null;

    /**
     * @type {MainMenuScreen}
     */
    this._mainMenuScreen = new OFP.ui.MainMenuScreen(this._screensStack, this._settings, this._audio);

    /**
     * Start parameters passed in url query
     * @type {Object}
     */
    this._startParameters = wglb.getUrlQuery();

    this._introData = {};
};

/**
 * Starts the game
 */
OFP.Game.prototype.start = function() {
    this._initRenderer();
    this._loadWorld();
    wglb.showDiv(document.getElementById("popupContainer"), false);
    this._showMainMenu();
    if (!this._startParameters.nosplash) {
        this._showSplashScreen();
    } else {
        //this._showMainMenu();
        this._showIntro();
    }

    if (!this._settings.arePersistent()) {
        this.showError("Settings are not persistent.<br>Please use browser supporting Local Storage and/or enable cookies and local storage in browser settings");
    }

    if (!this._audio.isSupported()) {
        this.showError("Audio is not supported.<br>Please use browser with HTML5 Audio support http://en.wikipedia.org/wiki/HTML5_Audio#Supported_browsers");
    }
};

OFP.Game.prototype._initRenderer = function() {
    this._renderer = new WGLE.Renderer(document.getElementById("webglCanvas"), window.innerWidth, window.innerHeight);

    window.onresize = (function(aEvent) {
        this._renderer.setSize(aEvent.target.innerWidth, aEvent.target.innerHeight);
    }).bind(this);

    // FPS display
    wglwDemo.EntityFactory.showFPS(this._renderer);

    this._renderer.start();
};

OFP.Game.prototype._showSplashScreen = function() {
    this._splashScreen = new OFP.ui.SplashScreen();
    this._screensStack.push(this._splashScreen);
};

OFP.Game.prototype._showIntro = function() {
    (!this._startParameters.nomusic) && this._audio.introTrack.play();

    this._introData.m113Pos = [3390, 31.40, 2942];
    this._introData.uralPos = [3370, 31.75, 2970];

    ga("send", "event", "screen", "show", 'Intro', new Date().getTime() - this._startTime);
    var that = this;
    setTimeout(function() {
        ga("send", "event", "renderer", "render", 'FPS', that._renderer.fpsCounter.value);
    }, 2000);
};

OFP.Game.prototype._loadWorld = function() {
    this._dataProvider = new WGLW.provider.JsonDataProvider({
        dataUrl: "assets/models3d/world/intro/desert_island.json",
        textureUrl: "assets/models3d/world/intro/sand.jpg"
    });
    this._startTime = new Date().getTime();
    this._worldModel = new WGLW.World({
        dataProvider: this._dataProvider,
        viewDistance: this._settings.getViewDistance()
    }, this._onWorldLoaded.bind(this));
};

OFP.Game.prototype._onWorldLoaded = function() {
    console.debug("World is ready " + (new Date().getTime() - this._startTime) + "ms");

    // create objects
    wglwDemo.EntityFactory.createSkyBox(this._renderer, this._worldModel);
    wglwDemo.EntityFactory.createTerrain(this._renderer, this._worldModel/*, "assets/models3d/world/intro/desert_island.json", "assets/models3d/world/intro/sand.jpg"*/);
    wglwDemo.EntityFactory.createOcean(this._renderer, this._worldModel);
    wglwDemo.EntityFactory.createIntroEntities(this._renderer, this._worldModel);

    // view distance
    this._settings.addObserver(this._updateViewDistance.bind(this));
    this._updateViewDistance(this._settings, OFP.Settings.ViewDistanceProperty);

    if (this._splashScreen) {
        var that = this;
        this._splashScreen.finish(function() {
            that._screensStack.pop();

            that._showIntro();
            that._animateIntro();
        });
    } else {
        wglb.showDiv(document.getElementById("fullScreenContainer"), false);
        this._showIntro();
        this._animateIntro();
    }
};

OFP.Game.prototype._showMainMenu = function() {
    this._screensStack.push(this._mainMenuScreen);
};

OFP.Game.prototype._updateViewDistance = function(aSettings, aProperty) {
    if (aProperty === OFP.Settings.ViewDistanceProperty) {
        this._worldModel.environment.setViewDistance(this._settings.getViewDistance());
        this._renderer.viewDistance = this._worldModel.environment.viewDistance;
        this._renderer.updatePerspective();
    }
};

OFP.Game.prototype._animateIntro = function() {
    // 1st transition: from ural to m113 (~5m above ground)
    var target1 = vec3.create([0, 0, 0]);
    vec3.subtract(this._introData.m113Pos, this._introData.uralPos, target1);
    target1[1] -= 15;
    vec3.scale(target1, 1000);

    var startCamPos = vec3.create(this._introData.uralPos);
    startCamPos[1] += 5;
    startCamPos[2] += 30;

    var camPos1 = vec3.create(this._introData.m113Pos);
    camPos1[1] += 5;
    camPos1[2] += 7;

    // 2nd transition: looking at m113 from left to right
    var target2 = vec3.create(this._introData.m113Pos);
    target2[1] += 1;
    var camPos2 = vec3.create(this._introData.m113Pos);
    camPos2[0] -= 15;
    camPos2[1] += 1.5;
    camPos2[2] += 5;

    var camPos3 = vec3.create(camPos2);
    camPos3[0] += 20;
    camPos3[1] -= 2;

    this._renderer.camera.lookAt(startCamPos, target1);
    this._cameraAnimation = new WGLE.CameraAnimation(this._renderer.camera);
    this._cameraAnimation.addKeyframe({
        duration: 10000,
        cameraPosition: camPos1,//[3380, 40, 2975],
        targetPosition: target1
    });

    this._cameraAnimation.addKeyframe({
        duration: 0,
        cameraPosition: camPos2,
        targetPosition: target2
    });
    this._cameraAnimation.addKeyframe({
        duration: 10000,
        cameraPosition: camPos3,
        targetPosition: target2
    });

    var that = this;
    var applyFunction = function() {
        that._cameraAnimation.apply();
    };

    this._cameraAnimation.start(true, function() {
        var idx = that._renderer.frameFunctions.indexOf(applyFunction);
        console.debug("animation end handler, idx of renderer.frameFunction:"+idx);
        that._renderer.stop();// frameFunctions = that._renderer.frameFunctions.slice(idx, idx);
    });
    this._renderer.frameFunctions.push(applyFunction);

};

/**
 * Shows error popup
 * @param {String} aMessage - the message to show
 */
OFP.Game.prototype.showError = function(aMessage) {
    (new OFP.ui.Popup()).show(aMessage);
    console.error(aMessage);
    ga('send', 'event', 'popup', 'show', aMessage);
};
