﻿"use strict";

wglb.prepareNs("OFP");

/**
 * Fetches list of servers and their details
 * @constructor
 */
OFP.MultiplayerConnection = function() {
};

OFP.MultiplayerConnection._GAMENAME = "opflashr";
OFP.MultiplayerConnection._GAMEKEY = "Y3k7x1";
OFP.MultiplayerConnection._CONFIG_FILE_URL = "https://raw.githubusercontent.com/wiki/poweruser/ofpmonitor/masterservers.txt";
OFP.MultiplayerConnection._PORT = 28900;
OFP.MultiplayerConnection._PHP_PATH = "php/";
OFP.MultiplayerConnection._TIMEOUT = 8000;

/**
 * Fetches list of OFP servers (just IPv4 and ports)
 * @param {Function} aCallback called when list is ready (first callback is error or null and second is array of servers)
 */
OFP.MultiplayerConnection.prototype.fetchServersList = function(aCallback) {
    this._getMasterIP(this._fetchServersList, aCallback);
};

OFP.MultiplayerConnection.prototype._getMasterIP = function(aGetMasterIPCallback, aCallback) {
    var boundGetIP = this._getIPFromConfigFile.bind(this);
    wglb.getContentAsynch(
            OFP.MultiplayerConnection._CONFIG_FILE_URL,
            function(aFileContent) {
                var ip = boundGetIP(aFileContent);
                aGetMasterIPCallback(ip, aCallback);
            });
};

OFP.MultiplayerConnection.prototype._getIPFromConfigFile = function(aConfigFileContent) {
    var lines = aConfigFileContent.split('\n');
    var onlineBlock = false;
    for (var i = 0; i < lines.length; i++){
        var line = lines[i];
        onlineBlock = this._isOnlineBlock(line, onlineBlock);
        console.log(i, line);
        if (this._isAddress(line) && onlineBlock) {
            console.log("found ip or domain");
            return line;
        }
    }
    return undefined;
};

OFP.MultiplayerConnection.prototype._isOnlineBlock = function(aLineText, aPreviousValue) {
    aLineText = aLineText.toLowerCase();
    if (aLineText.indexOf("[online]") !== -1) {
        return true;
    }

    if (aLineText.indexOf("[offline]") !== -1) {
        return false;
    }

    return aPreviousValue;
};

OFP.MultiplayerConnection.prototype._isAddress = function(aText) {
    return aText.indexOf(".") > 0;
};

OFP.MultiplayerConnection.prototype._fetchServersList = function(aMasterIP, aCallback) {
    var url = OFP.MultiplayerConnection._PHP_PATH +
                    "get_ofp_servers.php?masterUrl=" + aMasterIP +
                    ":" + OFP.MultiplayerConnection._PORT;
    wglb.getContentAsynch(
            url,
            function (aJsonResponse, aError) {
                if (aError) {
                    aCallback(aError);
                    return;
                }

                try {
                    var response = JSON.parse(aJsonResponse);
                    aCallback(response.error, response.servers);
                } catch (e) {
                    console.error("Error when reading response from:", url, aJsonResponse, e);
                    aCallback(e)
                }
            }, OFP.MultiplayerConnection._TIMEOUT
    );
};

/**
 * Fetches OFP server's details
 * @param aAddress server address in format "ab.cd.ef.gh:port"
 * @param aCallback called when details are retrieved. As a callback parameter error and OFP.MultiplayerServer is passed or null in case of error
 */
OFP.MultiplayerConnection.prototype.fetchServerDetails = function (aAddress, aCallback) {
    var queryTime = new Date().getTime();
    wglb.getContentAsynch(
            OFP.MultiplayerConnection._PHP_PATH + "get_ofp_server_details.php?server=" + aAddress,
            function(aJsonResponse, aError) {
                if (aError) {
                    aCallback(aError);
                    return;
                }

                var ping = new Date().getTime() - queryTime;
                try {
                    var response = JSON.parse(aJsonResponse);
                    aCallback(response.error, response.error ? null : new OFP.MultiplayerServer(response.details, ping, aAddress));
                } catch (exception) {
                    aCallback("Couldn't fetch details for " + aAddress + ": " + exception.message +
                            ". Parsed string: " + aJsonResponse, null);
                }
            }, OFP.MultiplayerConnection._TIMEOUT);
};
