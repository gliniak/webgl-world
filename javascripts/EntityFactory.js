wglb.prepareNs("wglwDemo");

wglwDemo.EntityFactory = {
    SUN_DISTANCE: 100000, // TODO find nicer way to have sun position working

    /**
     * Creates X-Y-Z axis
     * @param {WGLE.Renderer} renderer object
     */
    createAxis: function(aRenderer) {
        var colors;
        colors = [[1, 0, 0, 1], [0, 0, 1, 1], [1, 0.5, 0, 1]];
        var positions;
        positions = [[-10, 0, 0, 10, 0, 0], [0, -10, 0, 0, 10, 0], [0, 0, -10, 0, 0, 10]];
        // 0X, 0Y and 0Z axis
        for (var i = 0; i < 3; ++i) {
            new WGLE.Entity(new WGLE.program.CustomColorProgram({
                renderer: aRenderer,
                color: colors[i]
            }), new WGLE.geometry.Geometry({
                drawMode: WebGLRenderingContext.LINES,
                gl: aRenderer.gl,
                positions: positions[i]
            }), aRenderer);
        }
    },

    /**
     * Creates net on the ground
     * @param {WGLE.Renderer} aRenderer
     * @param {WGLW.World} aWorld world model
     */
    createGroundNet: function(aRenderer, aWorld) {
        var program = new WGLE.program.CustomColorProgram({
            renderer: aRenderer,
            color: [0.9, 0.4, 0.2, 1],
            maxDistance: aWorld.environment.viewDistance
        });
        new WGLE.Entity(program, new WGLE.geometry.Net({gl: aRenderer.gl}), aRenderer);
        var cpyColors = this._copyNumberArray;
        var updateFog = function(sender, param) {
            cpyColors(aWorld.environment.fog.color, program.fogColor);
        };

        aWorld.addObserver(updateFog);
        updateFog();
    },

    /**
     * Creates a ground based on passed world
     * @param {WGLE.Renderer} aRenderer
     * @param {WGLW.World} aWorld world model
     */
    createTerrain: function(aRenderer, aWorld) {
        var terrainData = aWorld.terrain;
        var program = new WGLE.program.TerrainProgram({
            renderer: aRenderer,
            waterLevel: 0,
            maxHeight: terrainData.maxHeight,
            minHeight: terrainData.minHeight,
            maxDistance: aWorld.environment.viewDistance
        });

        new WGLE.Entity(program, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: terrainData.vertices,
            indices: terrainData.indices,
            normals: terrainData.normalVectors,
            textureCoordinates: terrainData.textureCoordinates
        }), aRenderer, terrainData.textureUrl);

        // TODO remove this after fixing terrain shading
//        var terrainNormals = new WGLE.entities.NormalVectorsEntity({
//            positions: terrainData.positions,
//            normals: terrainData.normalVectors
//        }, aRenderer);

        var copyArray = this._copyNumberArray;
        var updateWorld = function(sender, param) {
            copyArray(aWorld.environment.fog.color, program.fogColor);
            copyArray(aWorld.sky.light.sunGlowColor, program.lightColor);
            copyArray(aWorld.sky.light.ambientColor, program.ambientColor);
            copyArray(aWorld.sky.light.sunPosition, program.lightPosition, wglwDemo.EntityFactory.SUN_DISTANCE);

            // TODO improve with Object.defineProperty(program, "maxDistance", set: ...)
            program.uniformsMap.uMaxDistance.value = aWorld.environment.viewDistance;
        };

        aWorld.addObserver(updateWorld);
        updateWorld();
    },

    /**
     * Creates water plate
     * @param {WGLE.Renderer} aRenderer
     * @param {WGLW.World} aWorld - world model
     */
    createOcean: function(aRenderer, aWorld) {
        var textureSize = 64;
        var texelDensity = 16;// texels / meter
        var waterSize = 128.0 * 50;
        var endCoordinate = (texelDensity / textureSize) * waterSize;
        var phongProgram = new WGLE.program.PhongProgram({
            renderer: aRenderer,
            maxDistance: aWorld.environment.viewDistance
        });
        new WGLE.Entity(phongProgram, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: [
                waterSize, 0.0, 0.0,
                0.0, 0.0, 0.0,
                0.0, 0.0, waterSize,
                waterSize, 0.0, waterSize
            ],
            indices: [
                0, 1, 2,
                0, 2, 3
            ], //TODO sprawdz triangle fan
            textureCoordinates: [
                endCoordinate, endCoordinate,
                0.0, endCoordinate,
                0.0, 0.0,
                endCoordinate, 0.0
            ],
            normals: [
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 1.0, 0.0
            ]
        }), aRenderer, "assets/water.png");

        var copyArray = this._copyNumberArray;
        var updateWorld = function(sender, param) {
            copyArray(aWorld.environment.fog.color, phongProgram.fogColor);
            copyArray(aWorld.sky.light.sunColor, phongProgram.lightColor);
            copyArray(aWorld.sky.light.ambientColor, phongProgram.ambientColor);
            copyArray(aWorld.sky.light.sunPosition, phongProgram.lightPosition, wglwDemo.EntityFactory.SUN_DISTANCE);

            phongProgram.uniformsMap.uMaxDistance.value = aWorld.environment.viewDistance;
        };

        aWorld.addObserver(updateWorld);
        updateWorld();
    },

    /**
     * Creates random triangles
     * @param {WGLE.Renderer} aRenderer
     * @param {WGLW.World} aWorld world model
     */
    createTestObjects: function(aRenderer, aWorld) {
        var program = new WGLE.program.CustomColorProgram({
            renderer: aRenderer,
            color: [0.35, 0.3, 0.2, 1],
            maxDistance: aWorld.environment.viewDistance
        });

        var startTime = new Date();
        // triangles
        for (var i = 0; i < 500; ++i) {
            var ent = new WGLE.Entity(program, new WGLE.geometry.Geometry({
                gl: aRenderer.gl,
                positions: [
                    1.0, 0.0, 0.0,
                    0.0, 1.0, 0.0,
                    -1.0, 0.0, 0.0
                ],
                indices: [0, 1, 2]
            }), aRenderer);
            mat4.translate(ent.mvMatrix, [Math.random() * 20 - 10, 0, Math.random() * -500], ent.mvMatrix);
        }
        console.debug("Creation time (500 entities):", new Date() - startTime);

        var phongProgram = new WGLE.program.PhongProgram({
            renderer: aRenderer,
            maxDistance: aWorld.environment.viewDistance
        });
        // texture entity
        new WGLE.Entity(phongProgram, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: [
                1.0, 0.0, -2.0,
                0.0, 2.0, -2.0,
                -1.0, 0.0, -2.0
            ],
            indices: [0, 1, 2],
            textureCoordinates: [
                1.0, 0.0,
                0.5, 1.0,
                0.0, 0.0
            ],
            normals: [
                0.0, 0.0, 1.0,
                0.0, 0.0, 1.0,
                0.0, 0.0, 1.0
            ]
        }), aRenderer, "assets/test.jpg");

        // tank
        var tankData = wglb.loadJSON("assets/models3d/vehicle/m1a1/m1a1.json");
        new WGLE.Entity(phongProgram, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: tankData.vertices,
            indices: tankData.indices,
            normals: tankData.normals,
            textureCoordinates: tankData.texCoords
        }), aRenderer, "assets/models3d/vehicle/m1a1/tank.jpg");

        // ball
        var ballData = wglb.loadJSON("assets/models3d/static/Ball.json");
        new WGLE.Entity(phongProgram, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: ballData.vertices,
            indices: ballData.indices,
            normals: ballData.normals,
            textureCoordinates: ballData.texCoords
        }), aRenderer, "assets/models3d/vehicle/m1a1/tank.jpg");
        new WGLE.entities.NormalVectorsEntity({
            positions: ballData.vertices,
            normals: ballData.normals
        }, aRenderer);

        var copyArray = this._copyNumberArray;
        var updateWorld = function(sender, param) {
            copyArray(aWorld.environment.fog.color, program.fogColor);
            copyArray(aWorld.environment.fog.color, phongProgram.fogColor);
            copyArray(aWorld.sky.light.sunColor, phongProgram.lightColor);
            copyArray(aWorld.sky.light.ambientColor, phongProgram.ambientColor);
            copyArray(aWorld.sky.light.sunPosition, phongProgram.lightPosition, wglwDemo.EntityFactory.SUN_DISTANCE);

            phongProgram.uniformsMap.uMaxDistance.value = aWorld.environment.viewDistance;
        };

        aWorld.addObserver(updateWorld);
        updateWorld();
    },

    /**
     * Creates entities in Desert Island intro scene (1 M113, 1 truck, and 3 jeeps wrecks, some graves - no soldiers)
     * @param aRenderer
     * @param aWorld
     */
    createIntroEntities: function(aRenderer, aWorld) {
        var program = new WGLE.program.PhongProgram({
            renderer: aRenderer,
            maxDistance: aWorld.environment.viewDistance,
            enableSpecular: false
        });
        var graveData = wglb.loadJSON("assets/models3d/static/grave.json");
        var positions;
        positions = [[3390, 30, 2928], [3392, 30, 2928], [3394, 30, 2928]];
        for (var i = 0; i < 3; ++i) {
            var grave = new WGLE.Entity(program, new WGLE.geometry.Geometry({
                gl: aRenderer.gl,
                positions: graveData.vertices,
                indices: graveData.indices,
                normals: graveData.normals,
                textureCoordinates: graveData.texCoords
            }), aRenderer, "assets/models3d/static/grave.png");
            mat4.translate(grave.mvMatrix, positions[i], grave.mvMatrix);
        }

        var m113Data = wglb.loadJSON("assets/models3d/vehicle/m113_wreck/m113.json");
        var m113 = new WGLE.Entity(program, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: m113Data.vertices,
            indices: m113Data.indices,
            normals: m113Data.normals,
            textureCoordinates: m113Data.texCoords
        }), aRenderer, "assets/models3d/vehicle/m113_wreck/rust.jpg");
        mat4.translate(m113.mvMatrix, [3390, 31.40, 2942], m113.mvMatrix);
        mat4.rotateY(m113.mvMatrix, Math.PI / 16, m113.mvMatrix);

        var uralData = wglb.loadJSON("assets/models3d/vehicle/ural_wreck/ural.json");
        var ural = new WGLE.Entity(program, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: uralData.vertices,
            indices: uralData.indices,
            normals: uralData.normals,
            textureCoordinates: uralData.texCoords
        }), aRenderer, "assets/models3d/vehicle/ural_wreck/ural.jpg");
        mat4.translate(ural.mvMatrix, [3375, 31.75, 2970], ural.mvMatrix);
        mat4.rotateY(ural.mvMatrix, Math.PI * 1/4, ural.mvMatrix);

        var jeepData = wglb.loadJSON("assets/models3d/vehicle/jeep_wreck/jeep.json");
        var jeep = new WGLE.Entity(program, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: jeepData.vertices,
            indices: jeepData.indices,
            normals: jeepData.normals,
            textureCoordinates: jeepData.texCoords
        }), aRenderer, "assets/models3d/vehicle/m113_wreck/rust.jpg");
        mat4.translate(jeep.mvMatrix, [3395, 30.75, 2950], jeep.mvMatrix);
        mat4.rotateY(jeep.mvMatrix, Math.PI * 1/4, jeep.mvMatrix);

//        var uralNormals = new WGLE.entities.NormalVectorsEntity({
//          positions: uralData.vertices,
//          normals: uralData.normals,
//          normalsLength: 0.1
//        }, aRenderer);
//        mat4.translate(uralNormals.mvMatrix, [3370, 31.75, 2962], uralNormals.mvMatrix);
        //mat4.rotateY(uralNormals.mvMatrix, Math.PI * 3/4, uralNormals.mvMatrix);

//        var ballData = wglb.loadJSON("assets/models3d/static/Ball.json");
//        var ball = new WGLE.Entity(program, new WGLE.geometry.Geometry({
//            gl: aRenderer.gl,
//            positions: ballData.vertices,
//            indices: ballData.indices,
//            normals: ballData.normals,
//            textureCoordinates: ballData.texCoords
//        }), aRenderer, "assets/models3d/vehicle/m1a1/tank.jpg");
//        mat4.translate(ball.mvMatrix, [3375, 32, 2962], ball.mvMatrix);
//        var ballNormals = new WGLE.entities.NormalVectorsEntity({
//            positions: ballData.vertices,
//            normals: ballData.normals,
//            normalsLength: 0.1
//          }, aRenderer);
//        mat4.translate(ballNormals.mvMatrix, [3375, 32, 2962], ballNormals.mvMatrix);

       var copyArray = this._copyNumberArray;
       var updateWorld = function(sender, param) {
           copyArray(aWorld.environment.fog.color, program.fogColor);
           copyArray(aWorld.sky.light.sunColor, program.lightColor);
           copyArray(aWorld.sky.light.ambientColor, program.ambientColor);
           copyArray(aWorld.sky.light.sunPosition, program.lightPosition, wglwDemo.EntityFactory.SUN_DISTANCE);

           // TODO dirty hack for broken ural's normal vectors
           program.lightPosition[1] *= -1;
           program.lightPosition[0] *= -1;

           program.uniformsMap.uMaxDistance.value = aWorld.environment.viewDistance;
       };

        aWorld.addObserver(updateWorld);
        updateWorld();
    },

    /**
     * Creates sky box
     * @param {WGLE.Renderer} aRenderer
     * @param {WGLW.World} aWorld world model
     */
    createSkyBox: function(aRenderer, aWorld) {
        var program = new WGLE.program.SkyProgram({
            renderer: aRenderer,
            world: aWorld
        });
        var sphereEntity = new WGLE.Entity(program, new WGLE.geometry.Sphere({
            gl: aRenderer.gl,
            hemisphere: true,
            outer: false,
            radius: 1
        }), aRenderer);
        sphereEntity.depthMask = false;

        // set skybox's center always in camera position
        // TODO make camera observable and update this only when position changed
        aRenderer.frameFunctions.push(function() {
           var camPos = aRenderer.camera.position;
           sphereEntity.mvMatrix[12] = camPos[0];
           sphereEntity.mvMatrix[13] = camPos[1];
           sphereEntity.mvMatrix[14] = camPos[2];
        });

        aWorld.sky.addObserver(function(skySender, skyProperties) {
            var color = aWorld.sky.horizonColor;
            aRenderer.gl.clearColor(color[0], color[1], color[2], color[3]);
        });

        var color = aWorld.sky.horizonColor;
        aRenderer.gl.clearColor(color[0], color[1], color[2], color[3]);
    },

    /**
     * Shows rotating head
     * @param aRenderer {WGLE.Renderer}
     * @param {String} aHeadTexture
     */
    createHead: function(aRenderer, aHeadTexture) {
        var phongProgram = new WGLE.program.PhongProgram({
            renderer: aRenderer,
            lightPosition: [0, 0, 20],
            enableSpecular: false
        });
        var headData = wglb.loadJSON("assets/models3d/human/ofp_head.json");
        var head = new WGLE.Entity(phongProgram, new WGLE.geometry.Geometry({
            gl: aRenderer.gl,
            positions: headData.vertices,
            indices: headData.indices,
            normals: headData.normals,
            textureCoordinates: headData.texCoords
        }), aRenderer, aHeadTexture);

        return head;
    },

    /**
     * Requests update of FPS display
     * @param {WGLE.Renderer} renderer object
     */
    showFPS: function(aRenderer) {
        var fpsElement = document.getElementById("fps");
        aRenderer.fpsCounter.observers.push(function () {
            fpsElement.innerHTML = aRenderer.fpsCounter.value + " FPS";
        });
    },

    /**
     * Skips time (calls world.setDate) every second
     * @param {Number} aHowFaster - speed of time - 1 means normal, 60 means in 1 second it will jump 1 minute (3600 hour in 1 sec)
     * @param {WGLW.World} aWorld
     */
    makeTimeFaster: function(aHowFaster, aWorld) {
        // offset in ms
        var timeOffset = aHowFaster * 100;
        var dateToSet = new Date();
        var timeDiv = document.getElementById("time");

        window.setInterval(function() {
            dateToSet.setTime(dateToSet.getTime() + timeOffset);
            aWorld.setDate(dateToSet);
            timeDiv.innerHTML = dateToSet.getHours() + ":" + dateToSet.getMinutes();
        }, 100);
    },

    /**
     * Copies values from aSource to aDest array
     * @param {Array} aSource
     * @param {Array} aDest
     */
    _copyNumberArray: function(aSource, aDest, aScale) {
        aScale = aScale || 1.0;
        for (var i = 0, len = Math.min(aSource.length, aDest.length); i < len; ++i) {
            aDest[i] = aSource[i] * aScale;
        }
    }
};
