"use strict";

wglb.prepareNs("OFP");

/**
 * Encapsulates OFP Server data
 * @param {String} aDetails details response from server
 * @param {Number} aPing to OFP Server from website server
 * @param {String} aAddress containing IPv4:port
 * @constructor
 */
OFP.MultiplayerServer = function(aDetails, aPing, aAddress) {
    /**
     * Address of the server
     * @type String
     */
    this.address = this._fixAddress(aAddress);

    /**
     * Name of the server
     * @type String
     */
    this.name = "";

    /**
     * Server version
     * @type String
     */
    this.version = "";

    /**
     * Min. version required by the server
     * @type String
     */
    this.requiredVersion = "";

    /**
     * Current mission name
     * @type String
     */
    this.mission = "";

    /**
     * Map used in current mission
     * @type String
     */
    this.map = "";

    /**
     * Mods loaded by the server
     * @type String
     */
    this.mod = "";

    /**
     * Players count
     * @type String
     */
    this.playerCount = "";

    /**
     * Max allowed number of players
     * @type String
     */
    this.maxPlayers = "";

    /**
     * Game status e.g. "Playing" or "Waiting"
     * @type String
     */
    this.gameStatus = "";

    /**
     * Estimated time left for current mission
     * @type String
     */
    this.timeLeft = "";

    /**
     * The ping
     * @type Number
     */
    this.ping = aPing;

    this._parse(aDetails);
};

OFP.MultiplayerServer._GAMESTATE_CREATING = 2;
OFP.MultiplayerServer._GAMESTATE_WAITING = 6;
OFP.MultiplayerServer._GAMESTATE_DEBRIEFING = 9;
OFP.MultiplayerServer._GAMESTATE_SETTINGUP = 12;
OFP.MultiplayerServer._GAMESTATE_BRIEFING = 13;
OFP.MultiplayerServer._GAMESTATE_PLAYING = 14;

OFP.MultiplayerServer.prototype._parse = function(aDetails) {
    // examples
//"\\groupid\\261\\hostname\\ =[T.O.S]= (Islands+Xrofp mod) CTI server\\hostport\\8000\\mapname\\\\gametype\\\\numplayers\\2\\maxplayers\\22\\gamemode\\openplaying\\timeleft\\0\\param1\\1800\\param2\\90\\actver\\199\\reqver\\199\\mod\\RES;@80+Islands1.3;@xrofp\\equalModRequired\\0\\password\\0\\gstate\\2\\impl\\sockets\\platform\\win\\final\\\\queryid\\18470.1"
//"\\groupid\\261\\hostname\\[WGL5] 23rd ARMA CWA SERVER\\hostport\\2302\\mapname\\eden\\gametype\\crCTI 1.0 @BIS_RES0.5\\numplayers\\5\\maxplayers\\34\\gamemode\\openplaying\\timeleft\\0\\param1\\10\\param2\\1212\\actver\\199\\reqver\\199\\mod\\RES;@wgl5;@wgl5extra\\equalModRequired\\1\\password\\0\\gstate\\6\\impl\\sockets\\platform\\win\\final\\\\queryid\\97810.1"
//"\\groupid\\261\\hostname\\AMDRSN 100\/100MBIT 50FPS server [NL]\\hostport\\2303\\mapname\\\\gametype\\\\numplayers\\0\\maxplayers\\34\\gamemode\\openplaying\\timeleft\\0\\param1\\0\\param2\\0\\actver\\199\\reqver\\199\\mod\\RES\\equalModRequired\\0\\password\\0\\gstate\\2\\impl\\sockets\\platform\\win\\final\\\\queryid\\993177.1"
    var detailsArray = aDetails.split("\\");
    this.name = this._getValue(detailsArray, "hostname");
    this.version = this._fixVersion(this._getValue(detailsArray, "actver"));
    this.requiredVersion = this._fixVersion(this._getValue(detailsArray, "reqver"));
    this.mission = this._getValue(detailsArray, "gametype");
    this.map = this._getValue(detailsArray, "mapname");
    this.mod = this._getValue(detailsArray, "mod");
    this.playerCount = this._getValue(detailsArray, "numplayers");
    this.maxPlayers = this._getValue(detailsArray, "maxplayers");
    this.maxPlayers = this._fixMaxPlayers(this.maxPlayers);
    this.gameStatus = this._getGameStatus(this._getValue(detailsArray, "gstate"));
    this.timeLeft = this._getValue(detailsArray, "timeleft");
};

OFP.MultiplayerServer.prototype._fixVersion = function(aVersion) {
    return aVersion.substr(0, 1) + "." + aVersion.substr(1);
};

OFP.MultiplayerServer.prototype._fixAddress = function(aAddress) {
    var _lastDigit = parseInt(aAddress[aAddress.length - 1]) - 1;
    return aAddress.substring(0, aAddress.length - 1) + _lastDigit;
};

OFP.MultiplayerServer.prototype._fixMaxPlayers = function(aMaxPlayers) {
    // for some reason the reported max players is greater (+2) than real value
    return (parseInt(aMaxPlayers) - 2).toString();
};

OFP.MultiplayerServer.prototype._getValue = function(aDetailsArray, aKey) {
    var keyIndex = aDetailsArray.indexOf(aKey);
    return aDetailsArray[keyIndex + 1];
};

OFP.MultiplayerServer.prototype._getGameStatus = function(aGameStatusCode) {
    switch (parseInt(aGameStatusCode)) {
    case OFP.MultiplayerServer._GAMESTATE_CREATING:
        return "Creating";
    case OFP.MultiplayerServer._GAMESTATE_WAITING:
        return "Waiting";
    case OFP.MultiplayerServer._GAMESTATE_DEBRIEFING:
        return "Debriefing";
    case OFP.MultiplayerServer._GAMESTATE_SETTINGUP:
        return "Setting up";
    case OFP.MultiplayerServer._GAMESTATE_BRIEFING:
        return "Briefing";
    case OFP.MultiplayerServer._GAMESTATE_PLAYING:
        return "Playing";
    default:
        return "Unknown";
    }
};
