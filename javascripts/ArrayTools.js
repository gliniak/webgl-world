
Array.prototype.mergeUnique = function(newItems) {
    function isUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    var concatenation = this.concat(newItems);
    return concatenation.filter(isUnique);
};
